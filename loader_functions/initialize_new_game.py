import libtcodpy as libtcod

from components.equipment import Equipment
from components.fighter import Fighter
from components.inventory import Inventory
from components.equippable import Equippable
from components.level import Level
from game_messages import MessageLog
from game_states import GameStates
from map_objects.GameMap import GameMap
from render_functions import RenderOrder
from entity import Entity
from equipment_slots import EquipmentSlots


def get_constants():
    window_title = 'NDC 1st edition'
    game_version = 'v0.1.4'
    screen_width = 180
    screen_height = 96

    bar_width = 20
    panel_height = 22
    panel_y = screen_height - panel_height

    message_x = bar_width + 2
    message_width = screen_width - 2
    message_height = panel_height - 1

    map_width = 150
    map_height = 60

    room_max_size = 20
    room_min_size = 10
    max_rooms = 12

    fov_algorithm = 1
    fov_light_walls = True
    fov_radius = 10

    max_monsters_per_room = 10
    max_items_per_room = 10

    colors = {
        'dark_wall': libtcod.Color( 31, 23, 11 ),
        'dark_ground': libtcod.Color( 21, 16, 11  ),
        'light_wall': libtcod.Color(92, 65, 55),
        'light_ground': libtcod.Color(92, 74, 55),
        'light_wall_char': libtcod.Color(99, 84, 79),
        'dark_wall_char': libtcod.Color( 49, 39, 29 )
    }
    texture = {
        'brick_wall': '#',
        'floor_base': '.'
        
    }
    constants = {
        'window_title': window_title,
        'screen_width': screen_width,
        'screen_height': screen_height,
        'bar_width': bar_width,
        'panel_height': panel_height,
        'panel_y': panel_y,
        'message_x': message_x,
        'message_width': message_width,
        'message_height': message_height,
        'map_width': map_width,
        'map_height': map_height,
        'room_max_size': room_max_size,
        'room_min_size': room_min_size,
        'max_rooms': max_rooms,
        'fov_algorithm': fov_algorithm,
        'fov_light_walls': fov_light_walls,
        'fov_radius': fov_radius,
        'colors': colors,
        'texture': texture,
        'game_version': game_version
    }

    return constants

def get_game_variables(constants):
    fighter_component = Fighter(hp=100, defense=30, atkBonus=5, phBonus=1, strength=1, dexterity=1, intelligence=1)
    level_component = Level(current_level=1)
    inventory_component = Inventory(26)
    equipment_component = Equipment()
    player = Entity(0, 0, '@', libtcod.white, 'Player', blocks=True, render_order=RenderOrder.ACTOR,fighter=fighter_component,
                    inventory= inventory_component, level=level_component, equipment=equipment_component)
    entities = [player]

    equippable_component = Equippable(EquipmentSlots.MAIN_HAND, min_dmg=1, max_dmg=2)
    dagger = Entity(0, 0, '-', libtcod.sky, 'Dagger', equippable=equippable_component)
    player.inventory.add_item(dagger)
    player.equipment.toggle_equip(dagger)
    game_map = GameMap(constants['map_width'], constants['map_height'])
#    game_map.generateLevel(constants['max_rooms'], constants['room_min_size'], constants['room_max_size'],
#                      constants['map_width'], constants['map_height'], player, entities)
    game_map.make_map(constants['max_rooms'], constants['room_min_size'], constants['room_max_size'],
                      constants['map_width'], constants['map_height'], player, entities, MessageLog)

    message_log = MessageLog(constants['message_x'], constants['message_width'], constants['message_height'])
    game_state = GameStates.PLAYERS_TURN

    return player, entities, game_map, message_log, game_state
