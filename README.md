# Nordia Dungeon Crawl, Dungeon crawl "Nordia". Title is a WIP

"PNDC" is a work in progress, current features:

- [x] Several floors
- [x] Spell scrolls
- [x] Items!
- [x] Combat and enemies
- [x] Experience and player levels!
- [x] Ability Scores! (STR, DEX, INT, CON), each gives a passive at 3rd level
- [x] JSON saving and loading(bit broken, use serialize for now)

Work in progress:

- Ranged weapons
- Better visuals (Random colors, post effects and highlights for targetting)
- Create a web tool for creating items/monsters/abilities
- Unique monsters
- Unique levels to accompany the unique monsters
- BIG: Rework the damn level generation which wont work
- Fix FoV lightning!
- Nerf melee, currently dealing 300+ damage at level 5 (Ring of power + STR stacking)

The entire game is based in a fantasy world I've created, for more details surrounding the world check out [the World Anvil page](https://www.worldanvil.com/w/nordia-Zeay "World Anvil")