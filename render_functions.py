import libtcodpy as libtcod
import math
import sys
import random
from enum import Enum

from game_states import GameStates
from menus import inventory_menu, level_up_menu, character_screen, help_menu, race_menu

class RenderOrder(Enum):
    CORPSE = 1
    ITEM = 2
    ACTOR = 3
    STAIRS = 4
    def distance_to(self, other):
        dx = other.x - self.x
        dy = other.y - self.y
        return math.sqrt(dx ** 2 + dy ** 2)
def distanceToTile(player, x, y):
    dx = x - player.x
    dy = y - player.y
    return math.sqrt(dx ** 2 + dy ** 2)

TORCH_RADIUS = 10
SQUARED_TORCH_RADIUS = TORCH_RADIUS * TORCH_RADIUS
FOV_NOISE = None
FOV_TORCHX = 0.0
NOISE = libtcod.noise_new(1)

def render_all(con, panel, entities, player, game_map, fov_map, fov_recompute, message_log, screen_width, screen_height, bar_width,
               panel_height, panel_y, mouse, colors, texture, game_state, t_counter, off_con):
    global FOV_NOISE, FOV_TORCHX, NOISE, SQUARED_TORCH_RADIUS, TORCH_RADIUS
    if fov_recompute:
        for y in range(game_map.height):
            for x in range(game_map.width):
                visible = libtcod.map_is_in_fov(fov_map, x, y)
                wall = game_map.tiles[x][y].block_sight
                #game_map.tiles[x][y].explored = True
                if game_map.tiles[x][y].set_color is None:
                    if wall:
                        base = colors.get('dark_wall')
                        light = colors.get('light_wall')
                        base_char = colors.get('dark_wall_char')
                        light_char = colors.get('light_wall_char')
                    else:#Ground
                        base = colors.get('dark_ground')
                        light = colors.get('light_ground')

                    l_coef = libtcod.random_get_float(0, 0, 1.0)
                    light = libtcod.color_lerp(base, light, l_coef)
                    light_char = libtcod.color_lerp(base_char, light_char, l_coef)
                    game_map.tiles[x][y].set_color = light
                    game_map.tiles[x][y].fore_color = light_char
                if not visible:
                    if game_map.tiles[x][y].explored:
                        if wall:
                            libtcod.console_set_char_background(con, x, y, colors.get('dark_wall'), libtcod.BKGND_SET)
                            libtcod.console_set_default_foreground(con, colors.get('dark_wall_char'))
                            libtcod.console_put_char(con, x, y, texture.get('brick_wall'), libtcod.BKGND_SCREEN)
                        else:
                            libtcod.console_set_char_background(con, x, y, colors.get('dark_ground'), libtcod.BKGND_SET)
                            libtcod.console_set_default_foreground(con, game_map.tiles[x][y].fore_color)
                            libtcod.console_put_char(con, x, y, texture.get('floor_base'), libtcod.BKGND_SCREEN)
                else:
                    if wall:
                        libtcod.console_put_char(con, x, y, texture.get('brick_wall'), libtcod.BKGND_SCREEN)
                    else:
                        libtcod.console_set_char_background(con, x, y, game_map.tiles[x][y].set_color, libtcod.BKGND_SET)
                        libtcod.console_put_char(con, x, y, texture.get('floor_base'), libtcod.BKGND_SCREEN)
                    #Calculate FOV (Thanks ZMD for the idea):
                    torch_base = libtcod.Color(  166, 86, 0  )
                    FOV_TORCHX += 0.1
                    tdx = [FOV_TORCHX + 20.0]
                    dx = libtcod.noise_get(NOISE, tdx, libtcod.NOISE_SIMPLEX)* 1.5
                    tdx[0] += 30.0
                    dy = libtcod.noise_get(NOISE, tdx, libtcod.NOISE_SIMPLEX)*1.5
                    di = 0.005 * libtcod.noise_get(NOISE, [FOV_TORCHX], libtcod.NOISE_SIMPLEX)
                    radius = float((x - player.x + dx) * (x - player.x + dx) + (y - player.y + dy) * (y - player.y + dy))
                    if distanceToTile(player, x, y) < SQUARED_TORCH_RADIUS + di:
                        l = (SQUARED_TORCH_RADIUS - radius) / SQUARED_TORCH_RADIUS + di
                        if l < 0.0:
                            l = 0.0
                        elif l > 1.6:
                            l = 1.6
                    if wall:
                        light = libtcod.color_lerp(game_map.tiles[x][y].set_color, game_map.tiles[x][y].set_color+torch_base, l)
                        light_char = libtcod.color_lerp(game_map.tiles[x][y].fore_color, game_map.tiles[x][y].fore_color+torch_base, l)
                    else:
                        light = libtcod.color_lerp(game_map.tiles[x][y].set_color, game_map.tiles[x][y].set_color+torch_base, l)
                        light_char = libtcod.color_lerp(game_map.tiles[x][y].fore_color, game_map.tiles[x][y].fore_color+torch_base, l)
                    libtcod.console_set_char_foreground(con, x, y, light)


                    #game_map.tiles[x][y].explored = True
    entities_in_render_order = sorted(entities, key=lambda x: x.render_order.value)
    entities_in_sight = entities_in_render_order

    # Draw everything!!
    for entity in entities_in_render_order:
        draw_entity(con, entity, fov_map, game_map)
    libtcod.console_blit(con, 0, 0, screen_width, screen_height, 0, 0, 0)
    libtcod.console_set_default_background(panel, libtcod.black)
    libtcod.console_clear(panel)

    y = 1
    for message in message_log.messages:
        libtcod.console_set_default_foreground(panel, message.color)
        libtcod.console_print_ex(panel, message_log.x, y, libtcod.BKGND_NONE, libtcod.LEFT, message.text)
        y += 1
    """
    Renders the HP and XP bars.
    TODO: Add spell slots here.
    """
    render_bar(panel, 1, 1, bar_width, 'HP', player.fighter.hp, player.fighter.max_hp,
                libtcod.light_red, libtcod.darker_red)
    render_bar(panel, 1, 2, bar_width, 'Xp', player.level.current_xp, player.level.experience_to_next_level,
                libtcod.light_grey, libtcod.dark_grey)
    libtcod.console_print_ex(panel, 1, 3, libtcod.BKGND_NONE, libtcod.LEFT,
        'Dungeon Level: {0}'.format(game_map.dungeon_level))
    libtcod.console_print_ex(panel, 1, 4, libtcod.BKGND_NONE, libtcod.LEFT,
        'Press "m" for help!'.format(game_map.dungeon_level))
    libtcod.console_set_default_foreground(panel, libtcod.light_gray)
    libtcod.console_print_ex(panel, 1, 0, libtcod.BKGND_NONE, libtcod.LEFT,
                             get_names_under_mouse(mouse, entities, fov_map))

    #Render the nearby entities.
    name_count= 2
    libtcod.console_print_ex(panel, 150, 0, libtcod.BKGND_NONE, libtcod.CENTER, "In sight:")
    for name in render_visible(entities, fov_map):
        if name == "Player": #Small workaround to not always show the player as a nearby entity.
            libtcod.console_print_ex(panel, 130, 0, libtcod.BKGND_NONE, libtcod.CENTER, "")
        else:
            libtcod.console_print_ex(panel, 142, name_count, libtcod.BKGND_NONE, libtcod.LEFT, name)
            name_count += 1
    #BLIT the panel and skill effect console.
    libtcod.console_blit(panel, 0, 0, screen_width, panel_height, 0, 0, panel_y)

    """Skill effect console
    Set default background to screen, then draw. 
    """
    #libtcod.console_blit(off_con, 0, 0, screen_width, screen_height, 0, 0, 0)
    #libtcod.console_set_char_background(off_con, x, y, libtcod.black, libtcod.BKGND_NONE)
    #Inventory:
    if game_state in (GameStates.SHOW_INVENTORY, GameStates.DROP_INVENTORY):
        if game_state == GameStates.SHOW_INVENTORY:
            inventory_title = 'Press the key next to the item to use it, or Esc to close.\n'
        else:
            inventory_title = 'Press the key next to the item to drop it, or Esc to close.\n'
        
        inventory_menu(con, inventory_title, player.inventory, 50, screen_width, screen_height)
    elif game_state == GameStates.LEVEL_UP:
        level_up_menu(con, 'Level up! Increase an ability score!', player, 40, screen_width, screen_height)
    elif game_state == GameStates.CHARACTER_SCREEN:
        character_screen(player, 30, 10, screen_width, screen_height)
    elif game_state == GameStates.SHOW_HELP:
        help_menu(con, 'Help screen!', 140, screen_width, screen_height)
    if game_state == GameStates.SELECT_RACE:
        race_menu(con, 'Select a race!', 80, screen_width, screen_height)

def test_effect(off_con, effects, player, target, game_map, con):
    y = player.y
    
    
    for x in range(player.x, target.x):
        print(x)
        libtcod.console_blit(off_con, x, y, 100, 100, 0, 0, 0)
        libtcod.console_set_char_background(off_con, x, y, libtcod.black, libtcod.BKGND_NONE)
        libtcod.console_set_default_foreground(off_con, libtcod.red)
        libtcod.console_put_char(off_con, x, y, "X", libtcod.BKGND_SET)



def clear_all(con, entities):
    for entity in entities:
        clear_entity(con, entity)

def draw_entity(con, entity, fov_map, game_map):
    if libtcod.map_is_in_fov(fov_map, entity.x, entity.y) or (entity.stairs and game_map.tiles[entity.x][entity.y].explored):
        libtcod.console_set_default_foreground(con, entity.col)
        libtcod.console_put_char(con, entity.x, entity.y, entity.char, libtcod.BKGND_NONE)

def clear_entity(con, entity):
    #Destroy an entity
    libtcod.console_put_char(con, entity.x, entity.y, ' ', libtcod.BKGND_NONE)

def get_names_under_mouse(mouse, entities, fov_map):
    (x, y) = (mouse.cx, mouse.cy)

    names = [entity.name for entity in entities
             if entity.x == x and entity.y == y and libtcod.map_is_in_fov(fov_map, entity.x, entity.y)]
    names = ', '.join(names)

    return names.capitalize()


def render_bar(panel, x, y, total_width, name, value, maximum, bar_color, back_color):
    bar_width = int(float(value) / maximum * total_width)

    libtcod.console_set_default_background(panel, back_color)
    libtcod.console_rect(panel, x, y, total_width, 1, False, libtcod.BKGND_SCREEN)

    libtcod.console_set_default_background(panel, bar_color)
    if bar_width > 0:
        libtcod.console_rect(panel, x, y, bar_width, 1, False, libtcod.BKGND_SCREEN)
    
    libtcod.console_set_default_foreground(panel, libtcod.white)
    libtcod.console_print_ex(panel, int(x + total_width / 2), y, libtcod.BKGND_NONE, libtcod.CENTER,
                            '{0}: {1}/{2}'.format(name, value, maximum))

def render_visible(entities, fov_map):
    names = [entity.name for entity in entities
             if libtcod.map_is_in_fov(fov_map, entity.x, entity.y)]
    #names = ', '.join(names)

    #return names.capitalize()
    return names
