from random import randint
import libtcodpy as libtcod
import random
import json
from components.ai import BasicMonster, BasicRanged
from components.fighter import Fighter
from components.Item import Item
from map_objects.Stairs import Stairs
from item_functions import heal, scroll_lightning, scroll_fireball, scroll_confuse
from components.equipment import EquipmentSlots
from components.equippable import Equippable
from game_messages import Message, MessageLog
from map_objects.tile import Tile
from map_objects.rectangle import Rect
#from map_objects.cellRoom import cellRoom
from random_utils import random_choice_from_dict, from_dungeon_level
from entity import Entity
from render_functions import RenderOrder

#from faker import Faker

class GameMap:
    def __init__(self, width, height, dungeon_level=1):
        self.width = width
        self.height = height
        self.tiles = self.initialize_tiles()

        self.dungeon_level = dungeon_level

    def to_json(self):
        json_data = {
            'width': self.width,
            'height': self.height,
            'tiles': [[tile.to_json() for tile in tile_rows] for tile_rows in self.tiles]
        }
        return json_data

    @staticmethod
    def from_json(json_data):
        width = json_data.get('width')
        height = json_data.get('height')
        tiles_json = json_data.get('tiles')

        game_map = GameMap(width, height)
        game_map.tiles = Tile.from_json(tiles_json)

        return game_map

    def make_map(self, max_rooms, room_min_size, room_max_size, map_width, map_height, player,
        entities, message_log):
        #Create two rooms!
        rooms = []
        num_rooms = 0
        center_of_last_room_x = None
        center_of_last_room_y = None
        w = randint(room_min_size, room_max_size)
        h = randint(room_min_size, room_max_size)
        x = randint(0, map_width - w - 1)
        y = randint(0, map_height -h - 1)
        
        for r in range(max_rooms):
            w = randint(room_min_size, room_max_size)
            h = randint(room_min_size, room_max_size)
            #Random positions without going out of bounds
            x = randint(0, map_width - w - 1)
            y = randint(0, map_height -h - 1)
            new_room = Rect(x, y, w, h)
            self.drunk_dig(x, y, entities, message_log)
            #new_room = cellRoom(x, y, w, h)
            #Check for intersection
            for other_room in rooms:
                if new_room.intersect(other_room):
                    break
            else:
                #paint the map to the tiles
                self.create_room(new_room)
                #self.create_cell_room(new_room)
                #center coordinates of the new room, for later use
                (new_x, new_y) = new_room.center()

                center_of_last_room_x = new_x
                center_of_last_room_y = new_y

                if num_rooms == 0:
                    #First room, spawn player!
                    player.x = new_x
                    player.y = new_y
                else:
                    #Every room after the first connect with previous.

                    #center the coords
                    (prev_x, prev_y) = rooms[num_rooms - 1].center()

                    #A damn coin flip, who'd thought this be used here
                    if randint(0, 1)==1:
                        #First hori, the verti
                        self.create_h_tunnel(prev_x, new_x, prev_y)
                        self.create_v_tunnel(prev_y, new_y, new_x)
                    else:
                        self.create_v_tunnel(prev_y, new_y, prev_x)
                        self.create_h_tunnel(prev_x, new_x, prev_y)
                    self.place_entities(new_room, entities, message_log)
                rooms.append(new_room)
                num_rooms += 1
        stairs_component = Stairs(self.dungeon_level + 1)
        down_stairs = Entity(center_of_last_room_x, center_of_last_room_y, '>', libtcod.white, 'Stairs down',
            render_order=RenderOrder.STAIRS, stairs=stairs_component)
        print('Stairs!: ', down_stairs.x, down_stairs.y)
        entities.append(down_stairs)

    def initialize_tiles(self):
        tiles = [[Tile(True) for y in range(self.height)] for x in range(self.width)]
        return tiles
        
    def generate_rooms(self):
        #Select type of room to create
        #Generate it and return it

        if self.rooms:
            choice = random.random()

            if choice < self.squareRoomChance:
                room = self.generateRoomSquare()
            else:
                room = self.generateRoomCross()

        else: # First room. spawn player
            choice = random.random()
            if choice < self.cavernChance:
                room = self.generatRoomCavern()
            else:
                room = self.generateRoomSquare()
        return room


    def drunk_dig(self, x, y, entities, message_log):
        max_area = 0
        x = x
        y = y
        w = 2
        h = 2
        digging = 0
        while digging < 500:
            x = randint(x -1, x + 1)
            y = randint(y -1, y+1)
            print("x: " + str(x))
            print("y: " + str(y))
            while x >= self.width -1 or y >= self.height -1 or x <= 1 or y <= 1:
                if x <= 1:
                    x = randint(x+3, x+30)
                elif x >= self.width -1:
                    x = x -30
                if y <= 1:
                    y = randint(y+3, y+30)
                elif y >= self.height -1:
                    y = y - 30
                x = randint(x -1, x + 1)
                y = randint(y -1, y+1)

            direction = {
                'Left': self.tiles[x-max_area][y],
                'right': self.tiles[x+max_area][y],
                'up': self.tiles[x][y+max_area],
                'down': self.tiles[x][y-max_area],
                'top_left': self.tiles[x-max_area][y+max_area],
                'top_right':self.tiles[x+max_area][y+max_area],
                'down_left':self.tiles[x-max_area][y-max_area],
                'down_right': self.tiles[x+max_area][y-max_area]
            }
            dig_direction = random.choice(list(direction.values()))
            new_room = Rect(x, y, w, h)
            if digging == 30 or digging == 330:
                self.place_entities(new_room, entities, message_log, digger=True)
            
            
            dig_direction.blocked = False
            dig_direction.block_sight = False
            dig_direction.wall = False

            digging += 1

    def create_room(self, room):
    #Go through tiles in the rectangle to make them passable
        roomWidth = int((random.randint(8+2, 18))/2*2)
        roomHeight = int((random.randint(8+2, 18))/2*2)
        generate = True
        while(generate):
            offset = randint(0, 2)
            for x in range(room.x1, room.x2):
                for y in range(room.y1 , room.y2):
                    self.tiles[x][y].blocked = False
                    self.tiles[x][y].block_sight = False
                    self.tiles[x][y].wall = False
            generate = False
    
    def is_blocked(self, x, y):
        if self.tiles[x][y].blocked:
            return True
        
        return False
        
    def create_h_tunnel(self, x1, x2, y):
        for x in range(min(x1, x2), max(x1, x2) +1):
            self.tiles[x][y].blocked = False
            self.tiles[x][y].block_sight = False
            self.tiles[x][y].wall = False


    def create_v_tunnel(self, y1, y2, x):
        for y in range(min(y1, y2), max(y1, y2) +1):
            self.tiles[x][y].blocked = False
            self.tiles[x][y].block_sight = False
            self.tiles[x][y].wall = False

    def place_entities(self, room, entities, message_log, digger=False):
        #Random amount of monsters.
        #fake = Faker()
        max_monsters_per_room = from_dungeon_level([[7, 1], [14, 4], [19, 6]], self.dungeon_level)
        max_items_per_room = from_dungeon_level([[23, 1], [2, 2], [4, 4]], self.dungeon_level)

        num_of_monsters = randint(1, max_monsters_per_room)
        num_of_items = randint(0, max_items_per_room)
        json_monsters = open("monsters.json")
        monster_data = json.load(json_monsters)
        monster_chances = {}
        for monster in monster_data["monsters"]:
            #This adds monsters with their spawn chance
            # { monster_name: chance_to_spawn }
            monster_chances[monster["name"]] = from_dungeon_level(monster["spawn_chance"], self.dungeon_level) 
        if digger:
            num_of_monsters = randint(0, 2)
            num_of_items = randint(0, 2)
        item_chances = {
            'healing_potion': 80,
            'sword': from_dungeon_level([[100, 1], [25, 3]], self.dungeon_level),
            'shield': from_dungeon_level([[1, 1], [20, 3]], self.dungeon_level),
            'ring_of_power': from_dungeon_level([[500, 1], [6, 2], [7, 3]], self.dungeon_level),
            'amulet_of_vigour': from_dungeon_level([[205, 1], [2, 2], [3, 3]], self.dungeon_level),
            'lightning_scroll': from_dungeon_level([[25, 4]], self.dungeon_level),
            'fireball_scroll': from_dungeon_level([[25, 6]], self.dungeon_level),
            'leather_armour': from_dungeon_level([[33, 1], [39, 6], [40, 7]], self.dungeon_level),
            'burning_sword': from_dungeon_level([[1, 10], [2, 20]], self.dungeon_level)
        }
        for i in range(num_of_monsters):
            #pick a random location in the room to spawn a monster
            print(digger)
            if digger:
                x = room.x1
                y = room.y1
            else:
                x = randint(room.x1 + 1, room.x2 - 1)
                y = randint(room.y1 + 1, room.y2 - 1)
                while self.tiles[x][y].blocked == True:
                    x = randint(room.x1 + 1, room.x2 - 1)
                    y = randint(room.y1 + 1, room.y2 - 1)
            if not any([entity for entity in entities if entity.x == x and entity.y == y]):
                monster_choice = random_choice_from_dict(monster_chances)
                monster_name = monster_choice
                for m in monster_data["monsters"]:
                    if monster_name == m["name"]:
                        _hp = m["hp"]
                        _char = m["char"]
                        _defense = m["defense"]
                        _atkBonus = m["atkBonus"]
                        _ability_scores = m.get("ability_scores", None)
                        _phBonus = m["phBonus"]
                        _strength = 0
                        _constitution = 0
                        _dexterity = 0
                        _ai = m.get("ai", None)
                        _xp = m.get("xp", 0)
                        ai_component = None
                        if _ability_scores is not None:
                            for asi in _ability_scores:
                                if "strength" in asi:
                                    _strength = asi["strength"]
                                if "constitution" in asi:
                                    _constitution = asi["constitution"]
                                if "dexterity" in asi:
                                    _dexterity = asi["dexterity"]
                        if _ai is not None:
                            if "BasicMonster" in _ai:
                                ai_component = BasicMonster()
                #else:
                fighter_component = Fighter(_hp, _defense, _atkBonus, _phBonus, _xp, constitution=_constitution, strength=_strength, dexterity=_dexterity)
                #random_stats = int(randint(1, 8) + (self.dungeon_level*2.5))
                #fighter_component = Fighter(hp=18+random_stats, defense=35+random_stats, atkBonus=3+random_stats, phBonus=1+random_stats, xp=150+int(random_stats*3), constitution=6)

                monster = Entity(x, y, _char, libtcod.darker_green, monster_name, blocks=True, fighter=fighter_component,
                                render_order=RenderOrder.ACTOR, ai=ai_component)

                #print(random_stats)
                print(monster.name + ": " + str(x) + ", " + str(y))
                entities.append(monster)
        for i in range(num_of_items):
            if digger:
                x = room.x1
                y = room.y1
            else:
                x = randint(room.x1 + 1, room.x2 -1)
                y = randint(room.y1 + 3, room.y2-1)
                
            if not any([entity for entity in entities if entity.x == x and entity.y == y]):
                item_choice = random_choice_from_dict(item_chances)

                if item_choice == 'healing_potion':
                    item_component = Item(use_function=heal, amount=10)
                    item = Entity(x, y, '!', libtcod.darker_red, 'Healing Potion', render_order=RenderOrder.ITEM, item=item_component)

                elif item_choice == 'sword':
                    eqiuppable_component = Equippable(EquipmentSlots.MAIN_HAND, min_dmg=2, max_dmg=6)
                    item = Entity(x, y, '/', libtcod.white, 'Sword', equippable=eqiuppable_component)
                    
                elif item_choice == 'shield':
                    eqiuppable_component = Equippable(EquipmentSlots.OFF_HAND, defense_bonus=8, mitigation_type='physical', mitigation_value=8)
                    item = Entity(x, y, '[', libtcod.darker_orange, 'Small Shield', equippable=eqiuppable_component)
                elif item_choice == 'ring_of_power':
                    eqiuppable_component = Equippable(EquipmentSlots.ACCESSORY, str_bonus=2)
                    item = Entity(x, y, '=', libtcod.light_red, 'Ring of Might +1', equippable=eqiuppable_component)
                elif item_choice == 'amulet_of_vigour':
                    eqiuppable_component = Equippable(EquipmentSlots.ACCESSORY, con_bonus=10)
                    item = Entity(x, y, '"', libtcod.red, 'Amulet of Vigour', equippable=eqiuppable_component)

                elif item_choice == 'fireball_scroll':
                    item_component = Item(use_function=scroll_fireball, targeting=True, targeting_message=Message(
                        'Left click a tile for the fireball, right click to cancel', libtcod.light_cyan), damage=(randint(30, 60)), radius=4)
                    item = Entity(x, y, '?', libtcod.red, 'Fireball Scroll', render_order=RenderOrder.ITEM, item=item_component)
                elif item_choice == 'confusion_scroll':
                    item_component = Item(use_function=scroll_confuse, targeting=True, targeting_message=Message(
                        'Left-click an enemy to confuse it, or right-click to cancel.', libtcod.light_cyan))
                    item = Entity(x, y, '?', libtcod.light_pink, 'Confusion Scroll', render_order=RenderOrder.ITEM,
                                  item=item_component)
                elif item_choice == 'leather_armour':
                    equippable_component = Equippable(EquipmentSlots.ARMOR, defense_bonus=12, mitigation_type='physical', mitigation_value=4)
                    item = Entity(x, y, ']', libtcod.light_blue, 'Leather Armour', equippable=equippable_component)
                elif item_choice == 'burning_sword':
                    equippable_component = Equippable(EquipmentSlots.MAIN_HAND, phBonus=10, dmgType='fire', dmgValue=30)
                    item = Entity(x, y, '/', libtcod.gold, 'Burner sword', equippable=equippable_component)
                else:
                    item_component = Item(use_function=scroll_lightning, damage=45, maximum_range=5)
                    item = Entity(x, y, '?', libtcod.black, 'Scroll of lightning', render_order=RenderOrder.ITEM, item=item_component)

                entities.append(item)

    def next_floor(self, player, message_log, constants):
        self.dungeon_level += 1
        entities = [player]

        self.tiles = self.initialize_tiles()
        self.make_map(constants['max_rooms'], constants['room_min_size'], constants['room_max_size'],
                constants['map_width'], constants['map_height'], player, entities, message_log)
        player.fighter.heal(player.fighter.max_hp // 2)

        message_log.add_message(Message('You take a moment to rest, recovering some health', libtcod.light_red))
    
        return entities