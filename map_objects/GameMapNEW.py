from random import randint
import libtcodpy as libtcod
import random
import json
from components.ai import BasicMonster
from components.fighter import Fighter
from components.Item import Item
from map_objects.Stairs import Stairs
from item_functions import heal, scroll_lightning, scroll_fireball, scroll_confuse
from components.equipment import EquipmentSlots
from components.equippable import Equippable
from game_messages import Message, MessageLog
from map_objects.tile import Tile
from map_objects.rectangle import Rect
from random_utils import random_choice_from_dict, from_dungeon_level
from entity import Entity
from render_functions import RenderOrder
import XPLoaderPy3

class GameMap:
    def __init__(self, width, height, dungeon_level=1):
        self.width = width
        self.height = height
        #self.tiles = self.initialize_tiles()
        #self.tiles = []
        self.dungeon_level = dungeon_level
        self.SQUARE_ROOM_MAX_SIZE = 12
        self.SQUARE_ROOM_MIN_SIZE = 6
        
        self.CROSS_ROOM_MAX_SIZE = 12
        self.CROSS_ROOM_MIN_SIZE = 6

        self.cavernChance = 0.40 # probability that the first room will be a cavern
        self.CAVERN_MAX_SIZE = 10 # max height an width

        self.wallProbability = 0.45
        self.neighbors = 4

        self.squareRoomChance = 0.2
        self.crossRoomChance = 0.15

        self.buildRoomAttempts = 500
        self.placeRoomAttempts = 20
        self.maxTunnelLength = 12

        self.includeShortcuts = True
        self.shortcutAttempts = 500
        self.shortcutLength = 5
        self.minPathfindingDistance = 50

    def to_json(self):
        json_data = {
            'width': self.width,
            'height': self.height,
            'tiles': [[tile.to_json() for tile in tile_rows] for tile_rows in self.tiles]
        }
        return json_data

    @staticmethod
    def from_json(json_data):
        width = json_data.get('width')
        height = json_data.get('height')
        tiles_json = json_data.get('tiles')

        game_map = GameMap(width, height)
        game_map.tiles = Tile.from_json(tiles_json)

        return game_map

    def make_map(self, max_rooms, room_min_size, room_max_size, map_width, map_height, player,
        entities, message_log):
        #Create two rooms!
        rooms = []
        num_rooms = 0
        center_of_last_room_x = None
        center_of_last_room_y = None
        pr_room = False
        for r in range(max_rooms):
            if randint(0, 1) == 0 and pr_room == False:
                #Create premade room
                print("Creating premade")
                self.tiles = []
                new_room = self.make_premade_room
                rooms.append(new_room)
                pr_room = True
            else: 
                print("Creating random")
                w = randint(room_min_size, room_max_size)
                h = randint(room_min_size, room_max_size)

                #Random positions without going out of bounds
                x = randint(0, map_width - w - 1)
                y = randint(0, map_height -h - 1)

                self.tiles = self.initialize_tiles()
                #Rectangle class makes rects easy
                new_room = Rect(x, y, w, h)

                #Check for intersection
                for other_room in rooms:
                    if new_room.intersect(other_room):
                        break
                else:
                    #paint the map to the tiles
                    self.create_room(new_room)
                    
                    #center coordinates of the new room, for later use
                    (new_x, new_y) = new_room.center()
                    center_of_last_room_x = new_x
                    center_of_last_room_y = new_y
                    if num_rooms == 0:
                        #First room, spawn player!
                        player.x = new_x
                        player.y = new_y
                    else:
                        #Every room after the first connect with previous.

                        #center the coords
                        (prev_x, prev_y) = rooms[num_rooms - 1].center()
                        #A damn coin flip, who'd thought this be used here
                        if randint(0, 1)==1:
                            #First hori, the verti
                            self.create_h_tunnel(prev_x, new_x, prev_y)
                            self.create_v_tunnel(prev_y, new_y, new_x)
                        else:
                            self.create_v_tunnel(prev_y, new_y, prev_x)
                            self.create_h_tunnel(prev_x, new_x, prev_y)
                        self.place_entities(new_room, entities, message_log)
                        rooms.append(new_room)
                        num_rooms += 1

        stairs_component = Stairs(self.dungeon_level + 1)
        down_stairs = Entity(center_of_last_room_x, center_of_last_room_y, '>', libtcod.white, 'Stairs down',
            render_order=RenderOrder.STAIRS, stairs=stairs_component)
        print('Stairs!: ', down_stairs.x, down_stairs.y)
        entities.append(down_stairs)
    def initialize_tiles(self):
        tiles = [[Tile(True) for y in range(self.height)] for x in range(self.width)]
        return tiles

    def make_premade_room():
#        json_data = open("map_starter.json")
#        data = json.load(json_data)
#        pre_map = data["Layout"]
#        height = len(pre_map)
#        width = len(pre_map[0])
#        #tiles = [[Tile(True) for y in range(self.height)] for x in range(self.width)]
#        map_ = [[Tile(True) for y in range(height) for x in range(width)]]
#        for y in range(height):
#            for x in range(width):
#                if pre_map[x][y] == '#':
#                    self.tiles[x][y].blocked = True
#                    self.tiles[x][y].block_sight = True
#                else:
#                    self.tiles[x][y].blocked = False
#                    self.tiles[x][y].block_sight = False
        XPLoaderPy3.load_xp_string("resources/xxx.xp")        
    def generate_rooms(self):
        #Select type of room to create
        #Generate it and return it

        if self.rooms:
            choice = random.random()

            if choice < self.squareRoomChance:
                room = self.generateRoomSquare()
            else:
                room = self.generateRoomCross()

        else: # First room. spawn player
            choice = random.random()
            if choice < self.cavernChance:
                room = self.generatRoomCavern()
            else:
                room = self.generateRoomSquare()
        return room

    def generateRoomSquare(self):
        roomWidth = int((random.randint(10+2, 18))/2*2)
        roomHeight = int((random.randint(10+2, 18))/2*2)
        generate = True
        while(generate):
            offset = randint(0, 2)
            for x in range(room.x1, room.x2):
                for y in range(room.y1 , room.y2):
                    self.tiles[x][y].blocked = False
                    self.tiles[x][y].block_sight = False
            generate = False
        
    def generatRoomCavern(self):
        while True:
            for x in range(room.x1, room.x2):
                for y in range(room.y1, room.y2):
                    self.tiles[x][y].blocked = False
                    self.tiles[x][y].block_sight = False

            for y in range(2, self.CAVERN_MAX_SIZE-2):
                for x in range(2, self.CAVERN_MAX_SIZE-2):
                    if random.random() >= self.wallProbability:
                            self.tiles[x][y].blocked = True
                            self.tiles[x][y].block_sight = True
            for i in range(4):
                for y in range(1, self.CAVERN_MAX_SIZE-1):
                    for x in range(1, self.CAVERN_MAX_SIZE-1):
                        
                        if self.getAdjacentWalls(x, y, self.tiles[x][y]) > self.neighbors:
                            self.tiles[x][y] = 0
                        elif self.getAdjacentWalls(x, y, self.tiles[x][y]) < self.neighbors:
                            self.tiles[x][y] = 1
        return None

    def create_room(self, room):
        roomWidth = int((random.randint(8+2, 18))/2*2)
        roomHeight = int((random.randint(8+2, 18))/2*2)
        generate = True
        while(generate):
            offset = randint(0, 2)
            for x in range(room.x1, room.x2):
                for y in range(room.y1 , room.y2):
                    self.tiles[x][y].blocked = False
                    self.tiles[x][y].block_sight = False
            generate = False
        for y in range(1, self.CAVERN_MAX_SIZE-1):
            for x in range(1, self.CAVERN_MAX_SIZE-1):
                self.tiles[x][y].blocked = False
                self.tiles[x][y].block_sight = False
    
    def is_blocked(self, x, y):
        if self.tiles[x][y].blocked:
            return True
        
        return False

    def create_h_tunnel(self, x1, x2, y):
        for x in range(min(x1, x2), max(x1, x2) +1):
            self.tiles[x][y].blocked = False
            self.tiles[x][y].block_sight = False


    def create_v_tunnel(self, y1, y2, x):
        for y in range(min(y1, y2), max(y1, y2) +1):
            self.tiles[x][y].blocked = False
            self.tiles[x][y].block_sight = False

    def place_entities(self, room, entities, message_log):
        #Random amount of monsters.
        max_monsters_per_room = from_dungeon_level([[3, 1], [5, 4], [7, 6]], self.dungeon_level)
        max_items_per_room = from_dungeon_level([[1, 1], [2, 2], [4, 4]], self.dungeon_level)

        num_of_monsters = randint(1, max_monsters_per_room)
        num_of_items = randint(0, max_items_per_room)
        monster_chances = {
            'orc': 80,
            'troll': from_dungeon_level([[15, 3], [30, 5], [60, 7]], self.dungeon_level),
            'ogre': from_dungeon_level([[5, 2], [30, 5], [40, 7]], self.dungeon_level),
            'ogre_boss': from_dungeon_level([[1, 2], [10, 5], [25, 10]], self.dungeon_level),
            'kobold': from_dungeon_level([[35, 1], [90, 10]], self.dungeon_level),
            'Demon Lord':from_dungeon_level([[90, 1], [1, 11]], self.dungeon_level)
        }
        item_chances = {
            'healing_potion': 80,
            'sword': from_dungeon_level([[10, 1], [25, 3]], self.dungeon_level),
            'shield': from_dungeon_level([[1, 1], [20, 3]], self.dungeon_level),
            'ring_of_power': from_dungeon_level([[5, 1], [6, 2], [7, 3]], self.dungeon_level),
            'amulet_of_vigour': from_dungeon_level([[25, 1], [2, 2], [3, 3]], self.dungeon_level),
            'lightning_scroll': from_dungeon_level([[25, 4]], self.dungeon_level),
            'fireball_scroll': from_dungeon_level([[25, 6]], self.dungeon_level),
            'leather_armour': from_dungeon_level([[33, 1], [39, 6], [40, 7]], self.dungeon_level),
            'burning_sword': from_dungeon_level([[1, 10], [2, 20]], self.dungeon_level)
        }
        for i in range(num_of_monsters):
            #pick a random location in the room to spawn a monster
            x = randint(room.x1 + 1, room.x2 - 1)
            y = randint(room.y1 + 1, room.y2 - 1)
            demon_lord_cap = 0
            if not any([entity for entity in entities if entity.x == x and entity.y == y]):
                monster_choice = random_choice_from_dict(monster_chances)
                if monster_choice == 'orc':
                    if self.dungeon_level > 5:
                        random_stats = int(randint(0, 5))
                    else:
                        random_stats = int(randint(0, 5))
                    fighter_component = Fighter(hp=6+random_stats, defense=25+random_stats, atkBonus=0+random_stats, phBonus=0+random_stats, xp=105+random_stats)
                    ai_component = BasicMonster()
                    
                    monster = Entity(x, y, 'o', libtcod.desaturated_green, 'Orc', blocks=True,
                                    render_order=RenderOrder.ACTOR, fighter=fighter_component, ai=ai_component)
                elif monster_choice == 'ogre':
                    random_stats = int(randint(1, 12))
                    fighter_component = Fighter(hp=50+random_stats, defense=35+random_stats, atkBonus=5+random_stats, phBonus=5+int((random_stats/2)), xp=350+random_stats)
                    ai_component = BasicMonster()

                    monster = Entity(x, y, '0', libtcod.desaturated_green, 'Ogre', blocks=True,
                                    render_order=RenderOrder.ACTOR, fighter=fighter_component, ai=ai_component)
                    
                elif monster_choice == 'ogre_boss':
                    random_stats = int(randint(0, 40))
                    fighter_component = Fighter(hp=120+random_stats, defense=40+random_stats, atkBonus=10, phBonus=5+int((random_stats/2)), xp=800+int(random_stats*2), constitution=10)
                    ai_component = BasicMonster()

                    monster = Entity(x, y, '0', libtcod.desaturated_green, 'Ogre Boss', bcol=libtcod.light_red, blocks=True, render_order=RenderOrder.ACTOR, fighter=fighter_component, ai=ai_component)
 
                elif monster_choice == 'Demon Lord':
                    if demon_lord_cap > 1:
                        monster_choice = 'Demon'
                    else:
                        demon_lord_cap += 1
                        random_stats = int(randint(1, 80))
                        fighter_component = Fighter(hp=300+random_stats, defense=60+int(random_stats / 3), atkBonus=25, phBonus=20+int(random_stats / 2), xp=1700+int(random_stats*5), constitution=30)
                        ai_component = BasicMonster()
                        
                        #message_log.add_message(Message('A chill goes down your spine as you descend to the next floor!', libtcod.light_pink))
                        monster = Entity(x, y, 'D', libtcod.red, 'Demon Lord', blocks=True, render_order=RenderOrder.ACTOR, fighter=fighter_component, ai=ai_component)
                elif monster_chances == 'kobold':
                    random_stats = int(randint(0, 3))
                    fighter_component = Fighter(hp=3+random_stats, defense=60+random_stats, atkBonus=0, phBonus=0+random_stats, xp=50+random_stats)
                    ai_component = BasicMonster()

                    monster = Entity(x, y, 'k', libtcod.light_red, 'Kobold', blocks=True, render_order=RenderOrder.ACTOR, fighter=fighter_component, ai=ai_component)
                elif monster_choice == 'Demon':                                              
                    random_stats = int(randint(1, 40))
                    fighter_component = Fighter(hp=200+random_stats, defense=50+int(random_stats / 2), atkBonus=20, phBonus=8+random_stats, xp=1200+int(random_stats*2), constitution=15)
                    ai_component = BasicMonster()
                    #MessageLog.add_message(Message('A chill goes down your spine as you decend into this floor.', libtcod.light_red))
                    monster = Entity(x, y, 'D', libtcod.red, 'Demon Lord', blocks=True, render_order=RenderOrder.ACTOR, fighter=fighter_component, ai=ai_component)
                else:
                    random_stats = int(randint(1, 8))
                    fighter_component = Fighter(hp=18+random_stats, defense=35+random_stats, atkBonus=3+random_stats, phBonus=1+random_stats, xp=150+int(random_stats*3), constitution=6)
                    ai_component = BasicMonster()

                    monster = Entity(x, y, 'T', libtcod.darker_green, 'Troll', blocks=True, fighter=fighter_component,
                                    render_order=RenderOrder.ACTOR, ai=ai_component)

                print(random_stats)
                print(monster.name + ": " + str(x) + ", " + str(y))
                entities.append(monster)
        for i in range(num_of_items):
            x = randint(room.x1 + 1, room.x2 -1)
            y = randint(room.y1 + 3, room.y2-1)
            
            if not any([entity for entity in entities if entity.x == x and entity.y == y]):
                item_choice = random_choice_from_dict(item_chances)

                if item_choice == 'healing_potion':
                    item_component = Item(use_function=heal, amount=10)
                    item = Entity(x, y, '!', libtcod.darker_red, 'Healing Potion', render_order=RenderOrder.ITEM, item=item_component)
                elif item_choice == 'sword':
                    eqiuppable_component = Equippable(EquipmentSlots.MAIN_HAND, phBonus=5)
                    item = Entity(x, y, '/', libtcod.white, 'Sword', equippable=eqiuppable_component)
                elif item_choice == 'shield':
                    eqiuppable_component = Equippable(EquipmentSlots.OFF_HAND, defense_bonus=8, mitigation_type='physical', mitigation_value=8)
                    item = Entity(x, y, '[', libtcod.darker_orange, 'Small Shield', equippable=eqiuppable_component)
                elif item_choice == 'ring_of_power':
                    eqiuppable_component = Equippable(EquipmentSlots.ACCESSORY_2, str_bonus=2)
                    item = Entity(x, y, '=', libtcod.light_red, 'Ring of Might +1', equippable=eqiuppable_component)
                elif item_choice == 'amulet_of_vigour':
                    eqiuppable_component = Equippable(EquipmentSlots.ACCESSORY_1, con_bonus=10)
                    item = Entity(x, y, '"', libtcod.red, 'Amulet of Vigour', equippable=eqiuppable_component)
                elif item_choice == 'fireball_scroll':
                    item_component = Item(use_function=scroll_fireball, targeting=True, targeting_message=Message(
                        'Left click a tile for the fireball, right click to cancel', libtcod.light_cyan), damage=(randint(30, 60)), radius=4)
                    item = Entity(x, y, '?', libtcod.red, 'Fireball Scroll', render_order=RenderOrder.ITEM, item=item_component)
                elif item_choice == 'confusion_scroll':
                    item_component = Item(use_function=scroll_confuse, targeting=True, targeting_message=Message(
                        'Left-click an enemy to confuse it, or right-click to cancel.', libtcod.light_cyan))
                    item = Entity(x, y, '?', libtcod.light_pink, 'Confusion Scroll', render_order=RenderOrder.ITEM,
                                  item=item_component)
                elif item_choice == 'leather_armour':
                    equippable_component = Equippable(EquipmentSlots.ARMOR, defense_bonus=12, mitigation_type='physical', mitigation_value=4)
                    item = Entity(x, y, ']', libtcod.light_blue, 'Leather Armour', equippable=equippable_component)
                elif item_choice == 'burning_sword':
                    equippable_component = Equippable(EquipmentSlots.MAIN_HAND, phBonus=10, dmgType='fire', dmgValue=30)
                    item = Entity(x, y, '/', libtcod.gold, 'Burner sword', equippable=equippable_component)
                else:
                    item_component = Item(use_function=scroll_lightning, damage=45, maximum_range=5)
                    item = Entity(x, y, '?', libtcod.black, 'Scroll of lightning', render_order=RenderOrder.ITEM, item=item_component)

                entities.append(item)

    def next_floor(self, player, message_log, constants):
        self.dungeon_level += 1
        entities = [player]

        self.tiles = self.initialize_tiles()
        self.make_map(constants['max_rooms'], constants['room_min_size'], constants['room_max_size'],
                constants['map_width'], constants['map_height'], player, entities, message_log)
        player.fighter.heal(player.fighter.max_hp // 2)

        message_log.add_message(Message('You take a moment to rest, recovering some health', libtcod.light_red))

        return entities