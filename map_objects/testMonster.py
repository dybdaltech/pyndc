import json
from random_utils import from_dungeon_level, random_choice_from_dict
def BasicMonster():
    #
    print("qwpoek")


def load_monsters():
    json_ = open("monsters.json")
    data = json.load(json_)
    monster_chances = {}
    for monster in data["monsters"]:
        #This adds monsters with their spawn chance
        # { monster_name: chance_to_spawn }
        monster_chances[monster["name"]] = from_dungeon_level(monster["spawn_chance"], 5) 
    i = 0
    while i < 100:
        #This returns a random monster based on It's spawn chance
        # 'Orc'
        monster_choice = random_choice_from_dict(monster_chances) 
        monster_name = monster_choice
        for m in data["monsters"]:
            if monster_name == m["name"]:
                _hp = m["hp"]
                _char = m["char"]
                _defense = m["defense"]
                _atkBonus = m["atkBonus"]
                _ability_scores = m.get("ability_scores", None)
                _phBonus = m["phBonus"]
                _strength = 0
                _constitution = 0
                _dexterity = 0
                _ai = m.get("ai", None)
                print(_ai)
                ai_component = None
                if _ability_scores is not None:
                    for asi in _ability_scores:
                        if "strength" in asi:
                            _strength = asi["strength"]
                        if "constitution" in asi:
                            _constitution = asi["constitution"]
                        if "dexterity" in asi:
                            _dexterity = asi["dexterity"]
                if _ai is not None:
                    if "BasicMonster" in _ai:
                        ai_component = BasicMonster()
                print("Name: " + str(monster_name))
                print("character: " + str(_char))
                print("Defense: " + str(_defense))
                print("attack: " + str(_atkBonus)) 
                print("STR: " + str(_strength))
                print("DEX: " + str(_dexterity))
                print("CON: "+ str(_constitution))
                print(ai_component)
                print(" ")                
        i += 1
        

load_monsters()