import libtcodpy as libtcod
from map_objects.tile import Tile
from random import randint

class Room:
    def __init__(self):
        self.tiles = self.initialize_tiles


    def create_square_room(self, width, height):
        generate = True
        while generate:
            for x in range(width):
                for y in range(height):
                    self.tiles[x][y].blocked = False
                    self.tiles[x][y].block_sight = False

    def create_cave_room(self, width, height):
        generate = True
        while generate:
            for x in range(width):
                for y in range(height):
                    self.tiles[x][y].blocked = False
                    self.tiles[x][y].block_sight = False

    def initialize_tiles(self):
        tiles = [[Tile(True) for y in range(self.height)] for x in range(self.width)]
        return tiles