import libtcodpy as libtcod
from map_objects.room import Room
import random

class Map:
    def __init__(self, dungeon_level=1):
        self.dungeon_level = dungeon_level
        self.rooms = []
    
        self.square_chance = 0.8
        self.cave_chance = 0.2

    def create_new_map(self, max_rooms):
        r = random.random()
        room_counts = 0
        for r in range(max_rooms):
            w = randint(10, 16)
            h = randint(11, 17)
            #
            x = randint(0, self.map_width - w - 1)
            y = randint(0, self.map_height - h - 1)

            choice = random.random()

            if choice < self.square_chance:
                room_counts += 1
                room = Room.create_square_room(w, h)
            elif choice < self.cave_chance:
                room_counts += 1
                room = Room.create_square_room(w, h)
            self.rooms.append(room)
            
        