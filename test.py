import libtcodpy as libtcod

import json
from random_utils import from_dungeon_level, random_choice_from_dict
import random
from components.equippable import Equippable
from components.equipment import EquipmentSlots
from entity import Entity
from components.Item import Item
#Ogre boss does no damage, check It's stats!
#Print min and max damage to character screen
#Print physical mitigation to character screen
def BasicMonster():
    #
    print("qwpoek")

def load_items():
    dungeon_level = 1
    items = []

    json_ = open("items.json")
    data = json.load(json_)
    item_types = ["weapons", "armours", "helmets", "accessories", "potions"]
    weapon_chances = {}
    armour_chances = {}
    helmet_chances = {}
    scroll_chances = {}
    potion_chances = {}
    accessory_chances = {}
    for weapon in data["weapons"]:
        weapon_chances[weapon["name"]] = from_dungeon_level(weapon["spawn_chance"], 3)
    for armor in data["armours"]:
        armour_chances[armor["name"]] = from_dungeon_level(armor["spawn_chance"], 3)
    for helmet in data["helmets"]:
        helmet_chances[helmet["name"]] = from_dungeon_level(helmet["spawn_chance"], 3)
    for scroll in data["scrolls"]:
        scroll_chances[scroll["name"]] = from_dungeon_level(scroll["spawn_chance"], 3)
    for potion in data["potions"]:
        potion_chances[potion["name"]] = from_dungeon_level(potion["spawn_chance"], 3)
    for accessory in data["accessories"]:
        accessory_chances[accessory["name"]] = from_dungeon_level(accessory["spawn_chance"], 3)
    item_probability = {
        'common': from_dungeon_level([[9000, 1]], dungeon_level),
        'uncommon': from_dungeon_level([[4050, 1], [5500, 10]], dungeon_level),
        'rare': from_dungeon_level([[700, 1], [750, 10]], dungeon_level),
        'veryrare': from_dungeon_level([[100, 1], [150, 15]], dungeon_level),
        'unique': from_dungeon_level([[5, 1], [7, 30]], dungeon_level),
        'legendary': from_dungeon_level([[1, 1], [5, 5], [6, 10]], dungeon_level)
    }
    i = 0
    l_count = 0
    vr_count = 0
    r_count = 0
    u_count = 0
    c_count = 0
    uq_count = 0
    while i < 100:
        i += 1
        r_type = random.choice(item_types)
        r_rarity = random_choice_from_dict(item_probability)
        if r_type == 'weapons':
            _item = random_choice_from_dict(weapon_chances)
            
            for it in data["weapons"]:
                if _item == it["name"] and it["rarity"] == r_rarity:
                    _name = it["name"]
                    _char = it["char"]
                    _slot = it["slot"]
                    _color = it["color"]
                    _stats = it["stats"]
                    _description = it["description"]
                    _max_dmg = _stats["max_damage"]
                    equippable_component = Equippable(EquipmentSlots.MAIN_HAND, min_dmg = _stats["min_damage"], max_dmg = _stats["max_damage"])
                    item = Entity(1, 1, _char, _color, _description, equippable=equippable_component)
                    items.append(item)
        elif r_type == 'armours':
            _item = random_choice_from_dict(armour_chances)
            for it in data["armours"]:
                if _item == it["name"] and it["rarity"] == r_rarity:
                    _name = it["name"]
                    _char = it["char"]
                    _slot = it["slot"]
                    _color = it["color"]
                    _stats = it["stats"]
                    _description = it["description"]
                    equippable_component = Equippable(EquipmentSlots.ARMOR, defense_bonus= _stats["defense_bonus"],
                    mitigation_type= _stats["mitigation_type"], mitigation_value= _stats["mitigation_type"])
                    item = Entity(1, 1, _char, _color, _description, equippable=equippable_component)
                    items.append(item)
        elif r_type == 'potions':
            _item = random_choice_from_dict(potion_chances)
            for it in data["potions"]:
                if _item == it["name"] and it["rarity"] == r_rarity:
                    _name = it["name"]
                    _char = it["char"]
                    _color = it["color"]
                    _stats = it["stats"]
                    item_component = Item(use_function=_stats["use_function"], amount=_stats["amount"])
                    item = Entity(1, 1, _char, _color, _name, item=item_component)
                    items.append(item)
        elif r_type =='accessories':
            _item = random_choice_from_dict(accessory_chances)
            for it in data["accessories"]:
                if _item == it["name"] and it["rarity"] == r_rarity:
                    _name = it["name"]
                    _char = it["char"]
                    _color = it["color"]
                    _stats = it["stats"]
                    equippable_component = Equippable(EquipmentSlots.ACCESSORY, )

#                if item_choice == 'healing_potion':
#                    item_component = Item(use_function=heal, amount=10)
#                    item = Entity(x, y, '!', libtcod.darker_red, 'Healing Potion', render_order=RenderOrder.ITEM, item=item_component)

    print(items)
        


def load_monsters():
    json_ = open("monsters.json")
    data = json.load(json_)
    monster_chances = {}
    for monster in data["monsters"]:
        #This adds monsters with their spawn chance
        # { monster_name: chance_to_spawn }
        monster_chances[monster["name"]] = from_dungeon_level(monster["spawn_chance"], 5) 
    i = 0
    while i < 100:
        #This returns a random monster based on It's spawn chance
        # 'Orc'
        monster_choice = random_choice_from_dict(monster_chances) 
        monster_name = monster_choice
        for m in data["monsters"]:
            if monster_name == m["name"]:
                _hp = m["hp"]
                _char = m["char"]
                _defense = m["defense"]
                _atkBonus = m["atkBonus"]
                _ability_scores = m.get("ability_scores", None)
                _phBonus = m["phBonus"]
                _strength = 0
                _constitution = 0
                _dexterity = 0
                _ai = m.get("ai", None)
                print(_ai)
                ai_component = None
                if _ability_scores is not None:
                    for asi in _ability_scores:
                        if "strength" in asi:
                            _strength = asi["strength"]
                        if "constitution" in asi:
                            _constitution = asi["constitution"]
                        if "dexterity" in asi:
                            _dexterity = asi["dexterity"]
                if _ai is not None:
                    if "BasicMonster" in _ai:
                        ai_component = BasicMonster()
                print("Name: " + str(monster_name))
                print("character: " + str(_char))
                print("Defense: " + str(_defense))
                print("attack: " + str(_atkBonus)) 
                print("STR: " + str(_strength))
                print("DEX: " + str(_dexterity))
                print("CON: "+ str(_constitution))
                print(ai_component)
                print(" ")                
        i += 1
        

load_items()