from enum import Enum

class EquipmentSlots(Enum):
    MAIN_HAND = 1
    OFF_HAND = 2
    ACCESSORY_1 = 3
    BOOTS = 4
    ARMOR = 5
    HELMET = 6
    ACCESSORY_2 = 7
    ACCESSORY_3 = 8
    ACCESSORY = 9