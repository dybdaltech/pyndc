import libtcodpy as libtcod

from loader_functions.initialize_new_game import get_constants, get_game_variables
from entity import get_blocking_entities_at_location
from fov_functions import initialize_fov, recompute_fov
from input_handlers import handle_keys, handle_mouse, handle_main_menu
from death_functions import kill_monster, kill_player
from render_functions import clear_all, render_all, test_effect
from loader_functions.data_loaders import load_game, save_game
#from loader_functions.json_loaders import load_game, save_game
from menus import main_menu, message_box
from game_states import GameStates
from game_messages import Message
from network_components import Game_net
class walking_target():
    pass

def play_game(player, entities, game_map, message_log, game_state, con, panel, constants, off_con):
    fov_recompute = True
    fov_map = initialize_fov(game_map)

    key = libtcod.Key()
    mouse = libtcod.Mouse()
    network = Game_net.Game_net('localhost')
    network.initalize()
    network.send_message('Started new game.', 'somone')
    game_state = GameStates.SELECT_RACE
    previous_game_state = game_state

    targeting_item = None
    wait_counter = 0
    t_counter = 0
    while not libtcod.console_is_window_closed():
        libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, key, mouse)
        t_counter += 1
        if fov_recompute:
            recompute_fov(fov_map, player.x, player.y, constants['fov_radius'], constants['fov_light_walls'],
                constants['fov_algorithm'])
        render_all(con, panel, entities, player, game_map, fov_map, fov_recompute, message_log,
                   constants['screen_width'], constants['screen_height'], constants['bar_width'],
                   constants['panel_height'], constants['panel_y'], mouse, constants['colors'], constants['texture'], game_state, t_counter, off_con)
        libtcod.console_flush()
        clear_all(con, entities)
        clear_all(off_con, entities)
        #Input handling
        action = handle_keys(key, game_state)
        mouse_action = handle_mouse(mouse)
        move = action.get('move')
        pickup = action.get('pickup')
        wait = action.get('wait')
        show_inventory = action.get('show_inventory')
        inventory_index = action.get('inventory_index')
        drop_inventory = action.get('drop_inventory')
        take_stairs = action.get('take_stairs')
        level_up = action.get('level_up')
        show_character_screen = action.get('show_character_screen')
        show_help = action.get('show_help')
        exit = action.get('exit')
        fullscreen = action.get('fullscreen')
        move_floor_down = action.get('move_floor_down')
        cheat_level_up = action.get('cheat_level_up')

        race = action.get('race')
        left_click = mouse_action.get('left_click')
        right_click = mouse_action.get('right_click')

        player_turn_results = []
        #### DEBUG #####
        #objgraph.show_growth()
        #objgraph.show_most_common_types()
        #roots = objgraph.get_leaking_objects()
        #objgraph.show_most_common_types(objects=roots)
        ####################################################
        #KEYBOARD MOVEMENT
        if move and game_state == GameStates.PLAYERS_TURN:
            player.fighter.regen()
            dx, dy = move
            destination_x = player.x + dx
            destination_y = player.y + dy
            if not game_map.is_blocked(destination_x, destination_y):
                target = get_blocking_entities_at_location(entities, destination_x, destination_y)

                if target:
                    attack_results = player.fighter.attack(target)
                    player_turn_results.extend(attack_results)
                else:
                    player.move(dx, dy)

                    fov_recompute = True

                game_state = GameStates.ENEMY_TURN
        #MOUSE MOVEMENT|
        if left_click and game_state == GameStates.PLAYERS_TURN:
            player.fighter.regen()
            target_x, target_y = left_click
            target = walking_target()
            target.x = target_x
            target.y = target_y
            if target.x == player.x and target.y == player.y:
                #message_log.add_message(Message("You wait.", libtcod.yellow))
                wait = True
            elif not game_map.is_blocked(target.x, target.y):
                enemy_target = get_blocking_entities_at_location(entities, target.x, target.y)
                if enemy_target:
                    attack_results = player.fighter.attack(enemy_target)
                    player_turn_results.extend(attack_results)
                else:
                    player.move_astar(target, entities, game_map)
                    test_effect(off_con, "somethign", player, target, game_map, con)
                    game_state = GameStates.ENEMY_TURN
            else:
                player.move_astar(target, entities, game_map)
                game_state = GameStates.ENEMY_TURN
            game_state = GameStates.ENEMY_TURN
        elif pickup and game_state == GameStates.PLAYERS_TURN:
            for entity in entities:
                if entity.item and entity.x == player.x and entity.y == player.y:
                    pickup_results = player.inventory.add_item(entity)
                    player_turn_results.extend(pickup_results)
                    break
                else:
                    message_log.add_message(Message('There is nothing here to pick up!', libtcod.yellow))
        elif wait:
            if wait_counter == 10 + player.level.current_level:
                message_log.add_message(Message('You take a moment to regain some health!', libtcod.light_green))
                if player.fighter.hp <= player.fighter.max_hp:
                    player.fighter.heal(player.fighter.max_hp * 0.05)
                wait_counter = 0
            else:
                wait_counter += 1
            game_state = GameStates.ENEMY_TURN
        if show_inventory:
            previous_game_state = game_state
            game_state = GameStates.SHOW_INVENTORY

        if drop_inventory:
            previous_game_state = game_state
            game_state = GameStates.DROP_INVENTORY

        if inventory_index is not None and previous_game_state != GameStates.PLAYER_DEAD and inventory_index < len(
            player.inventory.items):
            item = player.inventory.items[inventory_index]
            
            if game_state == GameStates.SHOW_INVENTORY:

                player_turn_results.extend(player.inventory.use(item, entities=entities, fov_map=fov_map))
            elif game_state == GameStates.DROP_INVENTORY:
                player_turn_results.extend(player.inventory.drop_item(item))
        if show_help:
            previous_game_state = game_state
            game_state = GameStates.SHOW_HELP
        if game_state == GameStates.TARGETING:
            if left_click:
                target_x, target_y = left_click

                item_use_results = player.inventory.use(targeting_item, entities=entities, fov_map=fov_map,
                                                        target_x=target_x, target_y=target_y)
                player_turn_results.extend(item_use_results)
            elif right_click:
                player_turn_results.extend({'targeting_cancelled': True})
        if take_stairs and game_state == GameStates.PLAYERS_TURN:
            for entity in entities:
                if entity.stairs and entity.x == player.x and entity.y == player.y:
                    entities = game_map.next_floor(player, message_log, constants)
                    fov_map = initialize_fov(game_map)
                    fov_recompute = True
                    libtcod.console_clear(con)

                    break
            else:
                message_log.add_message(Message('There are no stairs here.', libtcod.yellow))
        if move_floor_down:
            entities = game_map.next_floor(player, message_log, constants)
            fov_map = initialize_fov(game_map)
            fov_recompute = True
            libtcod.console_clear(con)

        if cheat_level_up:
            player_turn_results.append({'xp': 99999})
        if level_up:
            if level_up == 'str':
                player.fighter.base_strength += 1
            elif level_up == 'dex':
                player.fighter.base_dexterity += 1
            elif level_up == 'con':
                player.fighter.base_constitution += 1
                player.fighter.base_max_hp += 25
                player.fighter.hp += 25
            elif level_up == 'int':
                player.fighter.base_intelligence += 5
            
            game_state = previous_game_state
        if show_character_screen:
            previous_game_state = game_state
            game_state = GameStates.CHARACTER_SCREEN
        if GameStates.SELECT_RACE:
            if race == 'Human':
                player.fighter.base_strength += 1
                player.fighter.base_dexterity += 1
                player.fighter.base_intelligence += 1
                player.fighter.base_constitution += 1
                message_log.add_message(Message("You wake up in a dark cavern, a small hooded lantern is next to you.", libtcod.white))
                message_log.add_message(Message("You pick up the lantern and your adventure starts..", libtcod.white))
                game_state = GameStates.PLAYERS_TURN
            elif race =='Half-elf':
                player.fighter.base_intelligence += 1
                player.fighter.base_dexterity += 1
                player.fighter.base_defense += 5
                message_log.add_message(Message("You wake up in a dark cavern, a small hooded lantern is next to you.", libtcod.white))
                message_log.add_message(Message("You pick up the lantern and your adventure starts..", libtcod.white))
                game_state = GameStates.PLAYERS_TURN
        if exit:
            if game_state in (GameStates.SHOW_INVENTORY, GameStates.DROP_INVENTORY, GameStates.CHARACTER_SCREEN, GameStates.SHOW_HELP):
                game_state = previous_game_state
            elif game_state == GameStates.TARGETING:
                player_turn_results.append({'targeting_cancelled': True})
            else:
                save_game(player, entities, game_map, message_log, game_state)

                return True

        if fullscreen:
            libtcod.console_set_fullscreen(not libtcod.console_is_fullscreen())

        for player_turn_result in player_turn_results:
            message = player_turn_result.get('message')
            dead_entity = player_turn_result.get('dead')
            item_added = player_turn_result.get('item_added')
            item_consumed = player_turn_result.get('consumed')
            item_dropped = player_turn_result.get('item_dropped')
            targeting = player_turn_result.get('targeting')
            targeting_cancelled = player_turn_result.get('targeting_cancelled')
            equip = player_turn_result.get('equip')
            xp = player_turn_result.get('xp')

            if message:
                message_log.add_message(message)
            if targeting_cancelled:
                game_state = previous_game_state

                message_log.add_message(Message('Targeting cancelled'))
            if dead_entity:
                if dead_entity == player:
                    message, game_state = kill_player(dead_entity)
                else:
                    message = kill_monster(dead_entity)
                message_log.add_message(message)
            if item_added:
                entities.remove(item_added)
                game_state = GameStates.ENEMY_TURN
            
            if item_consumed:
                game_state = GameStates.ENEMY_TURN
            
            if targeting:
                previous_game_state = GameStates.PLAYERS_TURN
                game_state = GameStates.TARGETING

                targeting_item = targeting

                message_log.add_message(targeting_item.item.targeting_message)

            if item_dropped:
                entities.append(item_dropped)
                game_state = GameStates.ENEMY_TURN
            if equip:
                equip_results = player.equipment.toggle_equip(equip)
                for equip_result in equip_results:
                    equipped = equip_result.get('equipped')
                    dequipped = equip_result.get('dequipped')
                    if equipped:
                        message_log.add_message(Message('You equipped the {0}'.format(equipped.name)))

                    if dequipped:
                        message_log.add_message(Message('You unequipped the {0}'.format(dequipped.name)))
                game_state = GameStates.ENEMY_TURN
            if xp:
                leveled_up = player.level.add_xp(xp)
                message_log.add_message(Message('You gain {0} experience!'.format(xp)))

                if leveled_up:
                    message_log.add_message(Message('You have reached level {0}'.format(player.level.current_level) + '!', libtcod.yellow))
                    previous_game_state = game_state
                    game_state = GameStates.LEVEL_UP

        if game_state == GameStates.ENEMY_TURN:
            for entity in entities:
                if entity.ai:
                    enemy_turn_results = entity.ai.take_turn(player, fov_map, game_map, entities)

                    for enemy_turn_result in enemy_turn_results:
                        message = enemy_turn_result.get('message')
                        dead_entity = enemy_turn_result.get('dead')

                        if message:
                            message_log.add_message(message)
                        if dead_entity:
                            if dead_entity == player:
                                message, game_state = kill_player(dead_entity)
                            else:
                                message = kill_monster(dead_entity)
                            message_log.add_message(message)

                    if game_state == GameStates.PLAYER_DEAD:
                        break
            else:
                game_state = GameStates.PLAYERS_TURN
        if t_counter == 100:
            t_counter = 0


def main():
    #
    constants = get_constants()

    libtcod.console_set_custom_font('terminal10x10.png', libtcod.FONT_LAYOUT_TCOD | libtcod.FONT_TYPE_GREYSCALE)
    libtcod.console_init_root(constants['screen_width'], constants['screen_height'], constants['window_title'], False)
    con = libtcod.console_new(constants['screen_width'], constants['screen_height'])
    off_con = libtcod.console_new(constants['screen_width'], constants['screen_height'], )
    #Panels
    panel = libtcod.console_new(constants['screen_width'], constants['panel_height'])
    player = None
    entities = []
    game_map = None
    message_log = None
    game_state = None

    show_main_menu = True
    show_load_error_message = False

    main_menu_background_image = libtcod.image_load('menu_background1.png')


    key = libtcod.Key()
    mouse = libtcod.Mouse()

    while not libtcod.console_is_window_closed():
        libtcod.sys_check_for_event(libtcod.EVENT_KEY_PRESS | libtcod.EVENT_MOUSE, key, mouse)

        if show_main_menu:
            main_menu(con, main_menu_background_image, constants['screen_width'],
                constants['screen_height'], constants['game_version'])
            if show_load_error_message:
                message_box(con, 'No adventures here!', 50, constants['screen_width'], constants['screen_height'])

            libtcod.console_flush()

            action = handle_main_menu(key)

            new_game = action.get('new_game')
            load_saved_game = action.get('load_game')
            exit_game = action.get('exit')

            if show_load_error_message and (new_game or load_saved_game or exit_game):
                show_load_error_message = False
            elif new_game:
                player, entities, game_map, message_log, game_state = get_game_variables(constants)
                #game_state = GameStates.PLAYERS_TURN
                game_state = GameStates.SELECT_RACE
                show_main_menu = False
            elif load_saved_game:
                try:
                    player, entities, game_map, message_log, game_state = load_game()
                    game_state = GameStates.PLAYERS_TURN

                    show_main_menu = False
                except OSError as e:
                    show_load_error_message = True
                    if e.errno == errno.ENOENT:
                        print("File not found!")
                    else:
                        raise
            elif exit_game:

                break
        else:
            libtcod.console_clear(con)
            libtcod.console_clear(off_con)
            play_game(player, entities, game_map, message_log, game_state, con, panel, constants, off_con)

            show_main_menu = True

            
if __name__ == '__main__':
    main()