import libtcodpy as libtcod
from random import randint
from game_messages import Message

class Fighter:
    def __init__(self, hp, defense, atkBonus, phBonus, xp=0, strength=0, dexterity=0, constitution=0,
                intelligence=0, totDef=0, fire_resistance=0, cold_resistance=0, lightning_resistance=0, physical_resistance=0, dmgType = None, dmgValue = 0,
                minDamage = 0, maxDamage = 0):
        self.base_max_hp = hp
        self.hp = hp
        self.base_defense = defense + dexterity
        self.base_atkBonus = atkBonus
        self.base_phBonus = phBonus
        self.xp = xp
        self.base_strength = strength
        self.base_dexterity = dexterity
        self.base_constitution = constitution
        self.base_intelligence = intelligence
        self.totDef = defense + dexterity
        self.fire_resistance = fire_resistance
        self.cold_resistance = cold_resistance
        self.lightning_resistance = lightning_resistance
        self.physical_resistance = physical_resistance
        self.dmgType = dmgType
        self.dmgValue = dmgValue
        self.minDamage = minDamage
        self.maxDamage = maxDamage

    def to_json(self):
        json_data = {
            'max_hp': self.max_hp,
            'hp': self.hp,
            'strength': self.strength,
            'dexterity': self.dexterity,
            'intelligence': self.intelligence,
            'constitution': self.constitution,
            'defense': self.defense
        }
        return json_data

    @staticmethod
    def from_json(json_data):
        max_hp = json_data.get('max_hp')
        hp = json_data.get('hp')
        strength = json_data.get('strength')
        dexterity = json_data.get('dexterity')
        intelligence = json_data.get('intelligence')
        constitution = json_data.get('constitution')
        defense = json_data.get('defense')

        fighter = Fighter(max_hp, defense, 1, 0, strength=strength, dexterity=dexterity, intelligence=intelligence, constitution=constitution,)
    @property
    def damage(self):
        if self.owner and self.owner.equipment:
            minDamage = self.owner.equipment.min_dmg_bonus + self.minDamage + 1 + self.strength
            maxDamage = self.owner.equipment.max_dmg_bonus + self.maxDamage + 4 + self.strength
        else:
            minDamage = 1 + self.strength
            maxDamage = 4 + self.strength
        return  (minDamage, maxDamage)
    @property
    def max_hp(self):
        if self.owner and self.owner.equipment:
            bonus = self.owner.equipment.max_hp_bonus
        else:
            bonus = 0

        return self.base_max_hp + bonus
    @property
    def strength(self):
        if self.owner and self.owner.equipment:
            bonus = self.owner.equipment.str_bonus
        else:
            bonus = 0
        return self.base_strength + bonus
    @property
    def dexterity(self):
        if self.owner and self.owner.equipment:
            bonus = self.owner.equipment.dex_bonus
        else:
            bonus = 0
        return self.base_dexterity + bonus

    @property
    def intelligence(self):
        if self.owner and self.owner.equipment:
            bonus = self.owner.equipment.int_bonus
        else:
            bonus = 0
        return self.base_intelligence + bonus

    @property
    def constitution(self):
        if self.owner and self.owner.equipment:
            bonus = self.owner.equipment.con_bonus
        else:
            bonus = 0
        return self.base_constitution + bonus
    @property
    def defense(self):
        if self.owner and self.owner.equipment:
            bonus = self.owner.equipment.defense_bonus
        else:
            bonus = 0
        return self.base_defense + bonus + self.dexterity

    
    @property
    def physical_mitigation(self):
        if self.owner and self.owner.equipment and 'physical' in self.owner.equipment.mitigation_type:
            bonus = self.owner.equipment.mitigation_value
        else:
            bonus = 0
        return self.physical_resistance + bonus
    @property
    def dmg_bonus_fire(self):
        if self.owner and self.owner.equipment and 'fire' in self.owner.equipment.dmgType:
            bonus = self.owner.equipment.dmgValue
        else:
            bonus = 0
        return bonus
    def take_damage(self, amount):  
        results = []
        taken = amount - self.physical_mitigation
        if taken <= 0:
            results.append({'message': Message('The attack dealt no damage!')})
        else:
            self.hp -= taken

        if self.hp <= 0:
            results.append({'dead': self.owner, 'xp': self.xp})    
        
        return results
    def take_elemental_damage(self, type, amount):
        results = []
        print("fire")
        if type is 'fire':
            taken = int(amount * self.fire_resistance)
            print(taken)
        if type is 'lightning':
            taken = int(amount * self.lightning_resistance)
        if type is 'cold':
            taken = int(amount * self.cold_resistance)
        
        if taken <= 0:
            results.append({'message': Message('The target took no extra damage')})
        else:
            self.hp -= taken
        if self.hp <= 0:
            results.append({'dead': self.owner, 'xp': self.xp})
    dex_atk_counter = 0
    def attack(self, target):
        results = []
        #### STRENGTH Bonus damage, higher damage higher strength
        if self.strength > 3:
            fighter_mod_str = self.strength*10
        else:
            fighter_mod_str = self.strength*5
        atk_times = 0
        fire_damage = self.dmg_bonus_fire
        ###########DEXTERITY Bonus attacks (flurry of blows), attacks two additional times per third#####
        if self.dexterity > 3:
            if self.dex_atk_counter == 3:
                while atk_times < 2:
                    self.dex_atk_counter = 0
                    fighter_mod_dex = self.dexterity*2
                    fighter_damage = fighter_mod_dex + fighter_mod_str
                    
                    atkModifier = fighter_mod_dex + fighter_mod_str + self.base_atkBonus

                    atkRoll = (randint(0, 100)) + atkModifier
                    dmgRoll = (randint(0+( int(fighter_damage / 2)), 6 + fighter_damage)) + self.base_phBonus + fire_damage
                    if atkRoll > target.fighter.defense:
                        if dmgRoll > 0:
                            results.append({'message': Message('{0} uses flurry of attacks on {1} for {2} hit points. ({3} VS {4})'.format(
                                self.owner.name.capitalize(), target.name, str(dmgRoll), str(atkRoll), target.fighter.defense), libtcod.white)})
                            results.extend(target.fighter.take_damage(dmgRoll))
                        #if fire_damage > 0:
                        #    results.append(target.fighter.take_elemental_damage('fire', fire_damage))
                    else:
                        results.append({'message': Message('{0} attacks {1} and missed! ({2} VS {3})'.format(
                            self.owner.name.capitalize(), target.name, str(atkRoll), target.fighter.defense), libtcod.white)})
                    atk_times += 1
            else:
                fighter_mod_dex = self.dexterity*2
                self.dex_atk_counter += 1
        else:
            fighter_mod_dex = self.dexterity*2

        
        fighter_damage = fighter_mod_dex + fighter_mod_str
        
        atkModifier = fighter_mod_dex + fighter_mod_str + self.base_atkBonus

        atkRoll = (randint(0, 100)) + atkModifier
        #dmgRoll = (randint(0+( int(fighter_damage / 2)), 6 + fighter_damage)) + self.base_phBonus + fire_damage
        (minDmg, maxDmg) = self.damage
        dmgRoll = int(randint(minDmg, maxDmg)) + self.base_phBonus + fire_damage
        #print(" ")
        #print(self.owner.name)
        #print("min: " +  str(minDmg))
        #print("max: " +  str(maxDmg))
        #print("roll: " + str(dmgRoll))
        #print("atk: " + str(atkRoll))
        
        if atkRoll > target.fighter.defense:
            if dmgRoll > 0:
                results.append({'message': Message('{0} attacks {1} for {2} hit points. ({3} VS {4})'.format(
                    self.owner.name.capitalize(), target.name, str(dmgRoll), str(atkRoll), target.fighter.defense), libtcod.white)})
                results.extend(target.fighter.take_damage(dmgRoll))
            #if fire_damage > 0:
             #   results.append(target.fighter.take_elemental_damage('fire', fire_damage))
        else:
            results.append({'message': Message('{0} attacks {1} and missed! ({2} VS {3})'.format(
                self.owner.name.capitalize(), target.name, str(atkRoll), target.fighter.defense), libtcod.white)})
        return results
    def ranged_attack(self, target):
        results = []
        damage = self.dexterity + int(randint(1, 4))
        atkModifier = self.dexterity
        atkRoll = (randint(1, 100))+atkModifier
        dmgRoll = (randint(0+(int(self.dexterity / 2)), 6 + self.dexterity)) + self.base_phBonus
        if atkRoll > target.fighter.defense:
            if dmgroll > 0:
                results.append({'message': Message('{0} Shoots at {1} for {2} hit points'.format(self.owner.name.capitalize(), target.name, str(dmgRoll)), libtcod.white)})
    def heal(self, amount):
        bonus_value = self.constitution
        self.hp += (randint(1, 8)) + amount + bonus_value

        if self.hp > self.max_hp:
            self.hp = self.max_hp

    def regen(self):
        self.hp += int(self.constitution / 3)

        if self.hp > self.max_hp:
            self.hp = self.max_hp