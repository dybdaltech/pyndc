class Equippable:
    def __init__(self, slot, str_bonus=0, dex_bonus=0, con_bonus=0, 
                int_bonus=0, max_hp_bonus=0, defense_bonus=0, mitigation_type=[],
                phBonus=0, mitigation_value=0, dmgType=None, dmgValue=0, min_dmg=0, max_dmg=0):
        self.slot = slot
        self.str_bonus = str_bonus
        self.dex_bonus = dex_bonus
        self.con_bonus = con_bonus
        self.int_bonus = int_bonus
        self.max_hp_bonus = max_hp_bonus
        self.defense_bonus = defense_bonus
        self.mitigation_type = mitigation_type
        self.mitigation_value =  mitigation_value
        self.phBonus = phBonus
        self.dmgType = dmgType
        self.dmgValue = dmgValue
        self.min_dmg_bonus = min_dmg
        self.max_dmg_bonus = max_dmg