from equipment_slots import EquipmentSlots

class Equipment:
    def __init__(self, main_hand=None, off_hand=None, accessory_2=None, accessory_1=None, accessory_3=None, amulet=None, boots=None, armor=None, helmet=None):
        self.main_hand = main_hand
        self.off_hand = off_hand
        self.accessory_1 = accessory_1
        self.accessory_2 = accessory_2
        self.boots = boots
        self.armor = armor
        self.helmet = helmet
        self.accessory_3 = accessory_3
        self.accessory_count = 0
    @property
    def max_hp_bonus(self):
        bonus = 0
        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.max_hp_bonus
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.max_hp_bonus
        if self.accessory_1 and self.accessory_1.equippable:
            bonus += self.accessory_1.equippable.max_hp_bonus
        if self.accessory_2 and self.accessory_2.equippable:
            bonus += self.accessory_2.equippable.max_hp_bonus
        if self.boots and self.boots.equippable:
            bonus += self.boots.equippable.max_hp_bonus
        if self.armor and self.armor.equippable:
            bonus += self.armor.equippable.max_hp_bonus
        if self.helmet and self.helmet.equippable:
            bonus += self.helmet.equippable.max_hp_bonus
        if self.accessory_3 and self.accessory_3.equippable:
            bonus += self.accessory_3.equippable.max_hp_bonus
        return bonus

    @property
    def defense_bonus(self):
        bonus = 0
        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.defense_bonus
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.defense_bonus
        if self.accessory_1 and self.accessory_1.equippable:
            bonus += self.accessory_1.equippable.defense_bonus
        if self.accessory_2 and self.accessory_2.equippable:
            bonus += self.accessory_2.equippable.defense_bonus
        if self.boots and self.boots.equippable:
            bonus += self.boots.equippable.defense_bonus
        if self.armor and self.armor.equippable:
            bonus += self.armor.equippable.defense_bonus
        if self.helmet and self.helmet.equippable:
            bonus += self.helmet.equippable.defense_bonus
        if self.accessory_3 and self.accessory_3.equippable:
            bonus += self.accessory_3.equippable.defense_bonus
        return bonus

    @property
    def str_bonus(self):
        bonus = 0

        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.str_bonus
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.str_bonus
        if self.accessory_1 and self.accessory_1.equippable:
            bonus += self.accessory_1.equippable.str_bonus
        if self.accessory_2 and self.accessory_2.equippable:
            bonus += self.accessory_2.equippable.str_bonus
        if self.boots and self.boots.equippable:
            bonus += self.boots.equippable.str_bonus
        if self.armor and self.armor.equippable:
            bonus += self.armor.equippable.str_bonus
        if self.helmet and self.helmet.equippable:
            bonus += self.helmet.equippable.str_bonus
        if self.accessory_3 and self.accessory_3.equippable:
            bonus += self.accessory_3.equippable.str_bonus
        return bonus
    @property
    def dex_bonus(self):
        bonus = 0

        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.dex_bonus
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.dex_bonus
        if self.accessory_1 and self.accessory_1.equippable:
            bonus += self.accessory_1.equippable.dex_bonus
        if self.accessory_2 and self.accessory_2.equippable:
            bonus += self.accessory_2.equippable.dex_bonus
        if self.boots and self.boots.equippable:
            bonus += self.boots.equippable.dex_bonus
        if self.armor and self.armor.equippable:
            bonus += self.armor.equippable.dex_bonus
        if self.helmet and self.helmet.equippable:
            bonus += self.helmet.equippable.dex_bonus
        if self.accessory_3 and self.accessory_3.equippable:
            bonus += self.accessory_3.equippable.dex_bonus
        return bonus
    @property
    def con_bonus(self):
        bonus = 0

        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.con_bonus
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.con_bonus
        if self.accessory_1 and self.accessory_1.equippable:
            bonus += self.accessory_1.equippable.con_bonus
        if self.accessory_2 and self.accessory_2.equippable:
            bonus += self.accessory_2.equippable.con_bonus
        if self.boots and self.boots.equippable:
            bonus += self.boots.equippable.con_bonus
        if self.armor and self.armor.equippable:
            bonus += self.armor.equippable.con_bonus
        if self.helmet and self.helmet.equippable:
            bonus += self.helmet.equippable.con_bonus
        if self.accessory_3 and self.accessory_3.equippable:
            bonus += self.accessory_3.equippable.con_bonus
        return bonus

    @property
    def int_bonus(self):
        bonus = 0

        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.int_bonus
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.int_bonus
        if self.accessory_1 and self.accessory_1.equippable:
            bonus += self.accessory_1.equippable.int_bonus
        if self.accessory_2 and self.accessory_2.equippable:
            bonus += self.accessory_2.equippable.int_bonus
        if self.boots and self.boots.equippable:
            bonus += self.boots.equippable.int_bonus
        if self.armor and self.armor.equippable:
            bonus += self.armor.equippable.int_bonus
        if self.helmet and self.helmet.equippable:
            bonus += self.helmet.equippable.int_bonus
        if self.accessory_3 and self.accessory_3.equippable:
            bonus += self.accessory_3.equippable.int_bonus
        return bonus

    @property
    def ph_bonus(self):
        bonus = 0

        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.phBonus
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.phBonus
        if self.accessory_1 and self.accessory_1.equippable:
            bonus += self.accessory_1.equippable.phBonus
        if self.accessory_2 and self.accessory_2.equippable:
            bonus += self.accessory_2.equippable.phBonus
        if self.boots and self.boots.equippable:
            bonus += self.boots.equippable.phBonus
        if self.armor and self.armor.equippable:
            bonus += self.armor.equippable.phBonus
        if self.helmet and self.helmet.equippable:
            bonus += self.helmet.equippable.phBonus
        if self.accessory_3 and self.accessory_3.equippable:
            bonus += self.accessory_3.equippable.phBonus
        return bonus
    @property
    def mitigation_type(self):
        type = []
        
        if self.main_hand and self.main_hand.equippable:
            type.append(self.main_hand.equippable.mitigation_type)
        if self.off_hand and self.off_hand.equippable:
            type.append(self.off_hand.equippable.mitigation_type)
        if self.accessory_1 and self.accessory_1.equippable:
            type.append(self.accessory_1.equippable.mitigation_type)
        if self.accessory_2 and self.accessory_2.equippable:
            type.append(self.accessory_2.equippable.mitigation_type)
        if self.boots and self.boots.equippable:
            type.append(self.boots.equippable.mitigation_type)
        if self.armor and self.armor.equippable:
            type.append(self.armor.equippable.mitigation_type)
        if self.helmet and self.helmet.equippable:
            type.append(self.helmet.equippable.mitigation_type)
        if self.accessory_3 and self.accessory_3.equippable:
            type.append(self.accessory_3.equippable.mitigation_type)
        return type
    @property
    def mitigation_value(self):
        bonus = 0

        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.mitigation_value
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.mitigation_value
        if self.accessory_1 and self.accessory_1.equippable:
            bonus += self.accessory_1.equippable.mitigation_value
        if self.accessory_2 and self.accessory_2.equippable:
            bonus += self.accessory_2.equippable.mitigation_value
        if self.boots and self.boots.equippable:
            bonus += self.boots.equippable.mitigation_value
        if self.armor and self.armor.equippable:
            bonus += self.armor.equippable.mitigation_value
        if self.helmet and self.helmet.equippable:
            bonus += self.helmet.equippable.mitigation_value
        if self.accessory_3 and self.accessory_3.equippable:
            bonus += self.accessory_3.equippable.mitigation_value
        return bonus
    
    @property
    def dmgType(self):
        type = []

        if self.main_hand and self.main_hand.equippable:
            type.append(self.main_hand.equippable.dmgType)
        if self.off_hand and self.off_hand.equippable:
            type.append(self.off_hand.equippable.dmgType)
        if self.accessory_1 and self.accessory_1.equippable:
            type.append(self.accessory_1.equippable.dmgType)
        if self.accessory_2 and self.accessory_2.equippable:
            type.append(self.accessory_2.equippable.dmgType)
        if self.boots and self.boots.equippable:
            type.append(self.boots.equippable.dmgType)
        if self.armor and self.armor.equippable:
            type.append(self.armor.equippable.dmgType)
        if self.helmet and self.helmet.equippable:
            type.append(self.helmet.equippable.dmgType)
        if self.accessory_3 and self.accessory_3.equippable:
            type.append(self.accessory_3.equippable.dmgType)
        return type
    
    @property
    def dmgValue(self):
        bonus = 0

        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.dmgValue
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.dmgValue
        if self.accessory_1 and self.accessory_1.equippable:
            bonus += self.accessory_1.equippable.dmgValue
        if self.accessory_2 and self.accessory_2.equippable:
            bonus += self.accessory_2.equippable.dmgValue
        if self.boots and self.boots.equippable:
            bonus += self.boots.equippable.dmgValue
        if self.armor and self.armor.equippable:
            bonus += self.armor.equippable.dmgValue
        if self.helmet and self.helmet.equippable:
            bonus += self.helmet.equippable.dmgValue
        if self.accessory_3 and self.accessory_3.equippable:
            bonus += self.accessory_3.equippable.dmgValue
        return bonus
    @property
    def min_dmg_bonus(self):
        bonus = 0

        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.min_dmg_bonus
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.min_dmg_bonus
        if self.accessory_1 and self.accessory_1.equippable:
            bonus += self.accessory_1.equippable.min_dmg_bonus
        if self.accessory_2 and self.accessory_2.equippable:
            bonus += self.accessory_2.equippable.min_dmg_bonus
        if self.boots and self.boots.equippable:
            bonus += self.boots.equippable.min_dmg_bonus
        if self.armor and self.armor.equippable:
            bonus += self.armor.equippable.min_dmg_bonus
        if self.helmet and self.helmet.equippable:
            bonus += self.helmet.equippable.min_dmg_bonus
        if self.accessory_3 and self.accessory_3.equippable:
            bonus += self.accessory_3.equippable.min_dmg_bonus
        return bonus
    @property
    def max_dmg_bonus(self):
        bonus = 0

        if self.main_hand and self.main_hand.equippable:
            bonus += self.main_hand.equippable.max_dmg_bonus
        if self.off_hand and self.off_hand.equippable:
            bonus += self.off_hand.equippable.max_dmg_bonus
        if self.accessory_1 and self.accessory_1.equippable:
            bonus += self.accessory_1.equippable.max_dmg_bonus
        if self.accessory_2 and self.accessory_2.equippable:
            bonus += self.accessory_2.equippable.max_dmg_bonus
        if self.boots and self.boots.equippable:
            bonus += self.boots.equippable.max_dmg_bonus
        if self.armor and self.armor.equippable:
            bonus += self.armor.equippable.max_dmg_bonus
        if self.helmet and self.helmet.equippable:
            bonus += self.helmet.equippable.max_dmg_bonus
        if self.accessory_3 and self.accessory_3.equippable:
            bonus += self.accessory_3.equippable.max_dmg_bonus
        return bonus
    def toggle_equip(self, equippable_entity):
        results = []

        slot = equippable_entity.equippable.slot
        print(equippable_entity.name)
        if slot == EquipmentSlots.MAIN_HAND:
            if self.main_hand == equippable_entity:
                self.main_hand = None
                results.append({'dequipped': equippable_entity})
            else:
                if self.main_hand:
                    results.append({'dequipped': equippable_entity})
                self.main_hand = equippable_entity
                results.append({'equipped': equippable_entity})

        if slot == EquipmentSlots.OFF_HAND:
            if self.off_hand == equippable_entity:
                self.off_hand = None
                results.append({'dequipped': equippable_entity})
            else:
                if self.off_hand:
                    results.append({'dequipped': equippable_entity})
                self.off_hand = equippable_entity
                results.append({'equipped': equippable_entity})

        if slot == EquipmentSlots.ACCESSORY:
            if self.accessory_1 is None:
                self.accessory_1 = equippable_entity
                results.append({'equipped': equippable_entity})
            elif self.accessory_2 is None:
                self.accessory_2 = equippable_entity
                results.append({'equipped': equippable_entity})
            elif self.accessory_3 is None:
                self.accessory_3 = equippable_entity
                results.append({'equipped': equippable_entity})
            elif (self.accessory_1, self.accessory_2, self.accessory_3) is not None:
                results.append({'message': Message('Your full of accessories!', libtcod.yellow)})


        if slot == EquipmentSlots.BOOTS:
            if self.boots == equippable_entity:
                self.boots = None
                results.append({'dequipped': equippable_entity})
            else:
                if self.boots:
                    results.append({'dequipped': equippable_entity})
                self.boots = equippable_entity
                results.append({'equipped': equippable_entity})

        if slot == EquipmentSlots.ARMOR:
            if self.armor == equippable_entity:
                self.armor = None
                results.append({'dequipped': equippable_entity})
            else:
                if self.armor:
                    results.append({'dequipped': equippable_entity})
                self.armor = equippable_entity
                results.append({'equipped': equippable_entity})

        if slot == EquipmentSlots.HELMET:
            if self.helmet == equippable_entity:
                self.helmet = None
                results.append({'dequipped': equippable_entity})
            else:
                if self.helmet:
                    results.append({'dequipped': equippable_entity})
                self.helmet = equippable_entity
                results.append({'equipped': equippable_entity})
        return results