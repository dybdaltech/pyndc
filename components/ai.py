import libtcodpy as libtcod
from random import randint
class BasicMonster:
    def take_turn(self, target, fov_map, game_map, entities):
        results = []
        monster = self.owner

        if libtcod.map_is_in_fov(fov_map, monster.x, monster.y):

            if monster.distance_to(target) >= 2:
                monster.move_astar(target, entities, game_map)
            elif target.fighter.hp > 0:
                attack_results = monster.fighter.attack(target)
                results.extend(attack_results)
        return results
    def to_json(self):
        json_data = {
            'name': self.__class__.__name__
        }
        return json_data
    
    @staticmethod
    def from_json():
        basic_monster = BasicMonster()


        return basic_monster

class BasicRanged:
    def take_turn(self, target, fov_map, game_map, entities):
        results =[]
        monster = self.owner

        if libtcod.map_is_in_fov(fov_map, monster.x, monster.y):
            if target.fighter.hp > 0 and monster.distance_to(target) >= 6:
                attack_results = monster.fighter.attack(target)
                results.extend(attack_results)
            elif monster.distance_to(target) <= 1:
                d_x = monster.x + int(randint(1, 2))
                d_y = monster.y + int(randint(1, 2))
                monster.move_towards(d_x, d_y, game_map, entities)
            elif monster.distance_to(target) >= 9 and monster.fighter.hp > monster.fighter.max_hp:
                monster.move_astar(target, entities, game_map)

        return results

class ConfusedMonster:

    def __init__(self, previous_ai, number_of_turns=10):
        self.previous_ai = previous_ai
        self.number_of_turns = number_of_turns
    
    def take_turn(self, target, fov_map, game_map, entities):
        if self.number_of_turns > 0:
            random_x = self.owner.x + randint(0, 2) -1
            random_y = self.owner.y + randint(0, 2) -1

            if random_x != self.owner.x and random_y != self.owner.y:
                self.owner.move_astar(random_x, random_y, game_map, entities)

            self.number_of_turns -= 1
        else:
            self.owner.ai = self.previous_ai
            results.append({
                'message': Message('The {0} is no longer confused!'.format(self.owner.name), libtcod.red)
            })

        return results
    def to_json(self):
        json_data = {
            'name': self.__class__.__name__,
            'previous_ai': self.previous_ai.__class__.__name__,
            'number_of_turns': self.number_of_turns
        }

        return json_data

    @staticmethod
    def from_json(json_data, owner):
        previous_ai_name = json_data.get('previous_ai')
        number_of_turns = json_data.get('number_of_turns')

        if previous_ai_name == 'BasicMonster':
            previous_ai = BasicMonster()
            previous_ai.owner = owner
        else:
            previous_ai = None

        confused_monster = ConfusedMonster(previous_ai, number_of_turns)

        return confused_monster