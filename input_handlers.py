import libtcodpy as libtcod


from game_states import GameStates

def handle_keys(key, game_state):
    if game_state== GameStates.PLAYERS_TURN:
        return handle_player_turn_keys(key)
    elif game_state == GameStates.PLAYER_DEAD:
        return handle_player_dead_keys(key)
    elif game_state == GameStates.TARGETING:
        return handle_targeting_keys(key)
    elif game_state in (GameStates.SHOW_INVENTORY, GameStates.DROP_INVENTORY):
        return handle_inventory_keys(key)
    elif game_state == GameStates.LEVEL_UP:
        return handle_level_up_menu_(key)
    elif game_state == GameStates.CHARACTER_SCREEN:
        return handle_character_screen(key)
    elif game_state == GameStates.SHOW_HELP:
        return handle_help_screen(key)
    elif game_state == GameStates.SELECT_RACE:
        return handle_race_selection(key)

    return {}

def handle_player_turn_keys(key):
    #Movement fuck
    key_char = chr(key.c)

    if key.vk == libtcod.KEY_UP or key_char == 'k':
        return {'move': (0, -1)}
    elif key.vk == libtcod.KEY_DOWN or key_char == 'j':
        return {'move': (0, 1)}
    elif key.vk == libtcod.KEY_LEFT or key_char == 'h':
        return {'move': (-1, 0)}
    elif key.vk == libtcod.KEY_RIGHT or key_char == 'l':
        return {'move': (1, 0)}
    elif key.vk == libtcod.KEY_SHIFT and libtcod.KEY_UP:
        return {'move': (0, -5)}
    elif key.vk == libtcod.KEY_SHIFT and libtcod.KEY_DOWN:
        return {'move': (0, 5)}
    elif key.vk == libtcod.KEY_SHIFT and libtcod.KEY_LEFT:
        return {'move': (-5, 0)}
    elif key.vk == libtcod.KEY_SHIFT and libtcod.KEY_RIGHT:
        print("OPQKWEOPQWE")
        return {'move': (5, 0)}
    elif key_char == 'y':
        return {'move': (-1, -1)}
    elif key_char == 'u':
        return {'move': (1, -1)}
    elif key_char == 'b':
        return {'move': (-1, 1)}
    elif key_char == 'n':
        return {'move': (1, 1)}
    elif key.vk == libtcod.KEY_INSERT:
        return {'move_floor_down': True}
    if key.vk == libtcod.KEY_ENTER and key.lalt:
        return {'fullscreen': True} #Fullscreen
    if key.vk == libtcod.KEY_END:
        print("OPQKWEOPQWE")
        return {'level_up': True}
    if key_char == 'g':
        print("pickup")
        return {'pickup': True}
    if key_char == 'i':
        return {'show_inventory': True}
    if key_char == 'd':
        return {'drop_inventory': True}
    if key.vk == libtcod.KEY_ENTER:
        return {'take_stairs': True}
    if key_char == 'c':
        return {'show_character_screen': True}
    if key_char == 'z':
        return {'wait': True}
    if key_char == 'm':
        return {'show_help': True}
    elif key.vk == libtcod.KEY_ESCAPE:
        ##Exit the game
        return {'exit': True}
    
    #No key pressed
    return {}

def handle_targeting_keys(key):
    if key.vk == libtcod.KEY_ESCAPE:
        return {'exit': True}
    
    return {}

def handle_player_dead_keys(key):
    key_char = chr(key.c)

    if key_char == 'i':
        return {'show_inventory':True}
    if key.vk == libtcod.KEY_ENTER and key.lalt:
        return {'fullscreen': True}
    elif key.vk == libtcod.KEY_ESCAPE:
        return {'exit': True}


    return {}

def handle_inventory_keys(key):
    index = key.c - ord('a')

    if index >= 0:
        return {'inventory_index': index}
    if key.vk == libtcod.KEY_ENTER and key.lalt:
        return {'fullscreen':True}
    elif key.vk == libtcod.KEY_ESCAPE:
        return {'exit': True}
    return {}

def handle_mouse(mouse):
    (x, y) = (mouse.cx, mouse.cy)

    if mouse.lbutton_pressed:
        print(x, y)
        return {'left_click': (x, y)}
    elif mouse.rbutton_pressed:
        return {'right_click': (x, y)}

    return {}

def handle_main_menu(key):
    key_char = chr(key.c)

    if key_char == 'a':
        return {'new_game': True}
    elif key_char == 'b':
        return {'load_game': True}
    elif key_char == 'c' or key.vk == libtcod.KEY_ESCAPE:
        return {'exit': True}
    return {}

def handle_level_up_menu_(key):
    if key:
        key_char = chr(key.c)

        if key_char == 'a':
            return { 'level_up': 'str'}
        elif key_char == 'b':
            return { 'level_up': 'dex'}
        elif key_char == 'c':
            return { 'level_up': 'con'}
        elif key_char == 'd':
            return { 'level_up': 'int'}
    return {}
def handle_character_screen(key):
    if key.vk == libtcod.KEY_ESCAPE:
        return { 'exit': True}
    return {}

def handle_help_screen(key):
    if key.vk == libtcod.KEY_ESCAPE:
        return { 'exit': True}
    return {}
def handle_race_selection(key):
    key_char = chr(key.c)

    if key_char == 'a':
        return {'race': 'Human'}
    elif key_char == 'b':
        return {'race':'Half-elf'}
    return {}