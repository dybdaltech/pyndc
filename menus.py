import libtcodpy as libtcod

def menu(con, header, options, width, screen_width, screen_height):
    if len(options) > 26: raise ValueError('Cannot have a menu with more than 26 options')

    # Total height of header
    header_height = libtcod.console_get_height_rect(con, 0 , 0, width, screen_height, header)
    height = len(options) + header_height

    #Create off screen console
    window = libtcod.console_new(width, height)

    #Print ze header
    libtcod.console_set_default_foreground(window, libtcod.white)
    libtcod.console_set_default_background(window, libtcod.black)
    libtcod.console_print_rect_ex(window, 0, 0, width, height, libtcod.BKGND_NONE, libtcod.LEFT, header)

    #Print everything
    y = header_height
    letter_index = ord('a')
    for option_text in options:
        text = '(' + chr(letter_index) + ' )' + option_text
        libtcod.console_print_ex(window, 0, y, libtcod.BKGND_NONE, libtcod.LEFT, text)
        y += 1
        letter_index += 1

    #Blit the contents
    x = int(screen_width / 2 -width /2 )
    y = int(screen_height/2 - height/2)
    libtcod.console_blit(window, 0, 0, width, height, 0, x, y, 1.0, 0.7)

def inventory_menu(con, header, inventory, inventory_width, screen_width, screen_height):
    #Show something for each item
    if len(inventory.items) == 0:
        options = ['.. Nothing here, press "G" to pick up items']

    else:
        options = [item.name for item in inventory.items]

    menu(con, header, options, inventory_width, screen_width, screen_height)

def main_menu(con, background_image, screen_width, screen_height, game_version):
    libtcod.image_blit_2x(background_image, 0, 0, 0)

    libtcod.console_set_default_foreground(0, libtcod.light_yellow)
    libtcod.console_print_ex(0, int(screen_width / 2), int((screen_height / 2)-7)  , libtcod.BKGND_NONE, libtcod.CENTER, 'NORDIA DUNGEON CRAWL')
    libtcod.console_print_ex(0, int(screen_width / 2), int(screen_height / 2), libtcod.BKGND_NONE, libtcod.CENTER, 'By Zeay, aka Dybdaltech')
    libtcod.console_print_ex(0, int(screen_width / 2), int(2), libtcod.BKGND_NONE, libtcod.CENTER, game_version)

    menu(con, '', ['New Adventure', 'Continue your adventure!', 'Quit'], 24, screen_width, screen_height-5)

def message_box(con, header, width, screen_width, screen_height):
    menu(con, header, [], screen_width, screen_height)

def level_up_menu(con, header, player, menu_width, screen_width, screen_height):
    options = ['Strength (+5 Damage)'.format(player.fighter.strength),
                'Dexterity (+2 Damage +2 Defense'.format(player.fighter.dexterity),
                'Constitution (+25 hit points'.format(player.fighter.constitution),
                'Intelligence (+5 Spell damage'.format(player.fighter.intelligence)]
    menu(con, header, options, menu_width, screen_width, screen_height)


def character_screen(player, character_screen_width, character_screen_height, screen_width, screen_height):
    window = libtcod.console_new(30, 90)

    libtcod.console_set_default_foreground(window, libtcod.white)
    libtcod.console_set_default_background(window, libtcod.black)
    libtcod.console_print_rect_ex(window, 0, 2, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                 libtcod.LEFT, 'Character information')
    libtcod.console_print_rect_ex(window, 0, 3, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                 libtcod.LEFT, 'Level: {0}'.format(player.level.current_level))
    libtcod.console_print_rect_ex(window, 0, 4, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                 libtcod.LEFT, 'Current Experience: {0}'.format(player.level.current_xp))
    libtcod.console_print_rect_ex(window, 0, 5, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                 libtcod.LEFT, 'Next level: {0}'.format(player.level.experience_to_next_level))
    libtcod.console_print_rect_ex(window, 0, 6, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                 libtcod.LEFT, 'Maximum hit points: {0}'.format(player.fighter.max_hp))
    libtcod.console_print_rect_ex(window, 0, 10, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                 libtcod.LEFT, 'Strength: {0}'.format(player.fighter.strength))
    libtcod.console_print_rect_ex(window, 0, 11, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                 libtcod.LEFT, 'Dexterity: {0}'.format(player.fighter.dexterity))
    libtcod.console_print_rect_ex(window, 0, 12, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                 libtcod.LEFT, 'Constitution: {0}'.format(player.fighter.constitution))
    libtcod.console_print_rect_ex(window, 0, 13, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                 libtcod.LEFT, 'Intelligence: {0}'.format(player.fighter.intelligence))
    libtcod.console_print_rect_ex(window, 0, 18, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                 libtcod.LEFT, 'AC: {0}'.format(player.fighter.defense))
    libtcod.console_print_rect_ex(window, 0, 19, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                 libtcod.LEFT, 'Phyiscal Mitigation: {0}'.format(player.fighter.physical_mitigation))
    libtcod.console_print_rect_ex(window, 0, 20, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                 libtcod.LEFT, 'Damage: {0}'.format(player.fighter.damage))
    ####################
    #####Equipment######
    ####################
    if player.equipment.main_hand:
        libtcod.console_print_rect_ex(window, 0, 22, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                    libtcod.LEFT, 'Main-hand: {0}'.format(player.equipment.main_hand.name))
    if player.equipment.off_hand:
        libtcod.console_print_rect_ex(window, 0, 23, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                    libtcod.LEFT, 'Off-hand: {0}'.format(player.equipment.off_hand.name))
    if player.equipment.accessory_1:
        libtcod.console_print_rect_ex(window, 0, 24, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                    libtcod.LEFT, 'Accessory 1: {0}'.format(player.equipment.accessory_1.name))
    if player.equipment.accessory_2:
        libtcod.console_print_rect_ex(window, 0, 25, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                        libtcod.LEFT, 'Accessory 2: {0}'.format(player.equipment.accessory_2.name))
    if player.equipment.accessory_3:
        libtcod.console_print_rect_ex(window, 0, 26, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                        libtcod.LEFT, 'Accessory 3: {0}'.format(player.equipment.accessory_3.name))
    if player.equipment.armor:
        libtcod.console_print_rect_ex(window, 0, 27, character_screen_width, character_screen_height, libtcod.BKGND_NONE,
                                    libtcod.LEFT, 'Armor: {0}'.format(player.equipment.armor.name))
    x = screen_width // 2 - character_screen_width // 2
    y = screen_height - character_screen_height
    character_screen_height = 60
    character_screen_width = 30
    libtcod.console_blit(window, 0, 0, character_screen_width, character_screen_height, 0, 0, 0, 1.0, 1.0)

def help_menu(con, header, menu_width, screen_width, screen_height):
    options = ['Press G to pick up items!',
                'If you reach 3 points in an attribute you gain a special effect', 
                'Open inventory with "i" and press the corresponding key to activate/equip/unequip items!',
                'Hold down Z to rest! Or press it. Resting for 10 + your level to regain some health!'
                ]
    #message_box(con, "Press G to pick up items!"             , 50, screen_width            , screen_height)
    #message_box(con, 'No adventures here!', 50, constants['screen_width'], constants['screen_height'])
    menu(con, header, options, menu_width, screen_width, screen_height)

def skill_menu(con, header, menu_width, screen_width, screen_height):
    options = [
        ''
    ]
def race_menu(con, header, menu_width, screen_width, screen_height):
    options = ['Human (+1 to all stats)',
                'Half-Elf (+1 to Int and Dex, start with high AC',
                'Dwarf (+2 strength, +25 base hit points',
                'Halfling (+1 Dexterity, start with very high AC',
                'Elf (+2 Dexterity, +1 intelligence)']
    menu(con, header, options, menu_width, screen_width, screen_height )